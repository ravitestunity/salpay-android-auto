package com.salarium.android.testautomation.pageobjects;

import com.salarium.android.testautomation.driver.Salarium;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Login {
	public AndroidDriver<AndroidElement> driver =Salarium.driver;
	Logger Log=Logger.getLogger(SendMoney.class);
	
	public void login(String UserName, String Password) throws InterruptedException{

	 new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='android.widget.EditText']")));
	   Thread.sleep(5000);
	   driver.findElement(By.xpath("//*[@class='android.widget.EditText']")).sendKeys(UserName);
	   try{
			((AndroidDriver<?>) driver).hideKeyboard();
			}catch(Exception e){
			
			}
	      new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/et_password']")));
	      driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/et_password']")).sendKeys(Password);
	      
	      Thread.sleep(2000);
	      try{
	      ((AndroidDriver<?>) driver).hideKeyboard();
	      }catch(Exception e){
	    	  
	      }
	new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/btn_login']")));
	driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/btn_login']")).click();
	Thread.sleep(5000);
	((AndroidDriver<?>) driver).getKeyboard().sendKeys("1234");
	Thread.sleep(2000);
	try{
		((AndroidDriver<?>) driver).hideKeyboard();
		}catch(Exception e){
		
		}
	new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/btn_continue']")));
    driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/btn_continue']")).click();;
    new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='android:id/button1']")));
    driver.findElement(By.xpath("//*[@resource-id='android:id/button1']")).click();
    new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/billerTextView']")));
	MobileElement iselementpresent = driver.findElement((By.xpath("//*[@resource-id='com.salarium.android.salpay:id/billerTextView']")));
	if(iselementpresent.isDisplayed()){
		Log.info("Login successful");
	}else{
		Log.info("Login not successful");
	}
		
	}
	

}
