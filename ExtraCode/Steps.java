package com.salarium.android.testautomation.commonlibrary;


import com.salarium.android.testautomation.driver.Salarium;
import com.salarium.android.testautomation.pageobjects.Calculatefees;
import com.salarium.android.testautomation.pageobjects.Login;
import com.salarium.android.testautomation.pageobjects.SendMoney;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;


public class Steps {
    AndroidDriver<AndroidElement> driver= Salarium.driver;
	
	public String Password;
	public String UserName;
//@BeforeSuite
	@Given("^Open App$")
	public void open_App() throws Throwable {
		Salarium.setUp();
		
	}
	

		   
@Given("^Login using \"(.*?)\" and \"(.*?)\" click ok$")
public void login_using_and_click_ok(String UserName, String Password) throws Throwable {
 this.UserName=UserName;
 this.Password=Password;
	Login login= new Login();
   login.login(UserName, Password);
}
@Then("^Verify Send Money button on quick options$")
public void verify_Send_Money_button_on_quick_options()  {
	
    SendMoney sendmoneyoption = new SendMoney();

    sendmoneyoption.sendmoney_presence();

}

@Then("^Verify Send Money Options$")
public void verify_Send_Money_Options() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
    sendmoneyoption.sendmoney();
}

@Then("^Verify Back icon on the send money options page$")
public void verify_Back_icon_on_the_send_money_options_page() throws Throwable {
	
    SendMoney sendmoneyoption = new SendMoney();
    sendmoneyoption.sendmoneytoback();

}

@Then("^Verify send money to bank form$")
public void verify_send_money_to_bank_form() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();

    sendmoneyoption.sendmoney();
    sendmoneyoption.bankform();

    
}

@Then("^Verify Back icon on the send money form page$")
public void verify_Back_icon_on_the_send_money_form_page() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
    sendmoneyoption.bankformback();
	
    
}

@Then("^Verify Cancel icon on the send money page$")
public void verify_Cancel_icon_on_the_send_money_page() throws Throwable {
    //no cancel button available on bank form page
}

@Then("^Verify List of Banks$")
public void verify_List_of_Banks() throws Throwable {
	
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.banklist();
    
}

@Then("^Verify Additional Information icon on Account Name$")
public void verify_Additional_Information_icon_on_Account_Name() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.verifyadditionaliconaccountname();
    
}

@Then("^Verify Additional Information icon on Account Number$")
public void verify_Additional_Information_icon_on_Account_Number() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.verifyadditionaliconaccountnumber();
	//driver.findElement(By.xpath("//*[@class='android.widget.LinearLayout'][2]//*[@resource-id='com.salarium.android.salpay:id/descriptionImageView']")).click();
    
}

@Then("^Verify Amount Number format$")
public void verify_Amount_Number_format() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.verify_amount_number_format();
}

@Then("^Verify Clear icon when Amount text field is blank$")
public void verify_Clear_icon_when_Amount_text_field_is_blank() throws Throwable {
    
}

@Then("^Verify Clear icon when Amount is filled Account used:$")
public void verify_Clear_icon_when_Amount_is_filled_Account_used() throws Throwable {
    
}

@Then("^Tap CALCULATE FEES button when send money form is blank$")
public void tap_CALCULATE_FEES_button_when_send_money_form_is_blank() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.submit_calculate_fees();
	}

@Then("^Tap CALCULATE FEES button when Bank Name is not selected$")
public void tap_CALCULATE_FEES_button_when_Bank_Name_is_not_selected() throws Throwable {
	
   }

@Then("^Tap CALCULATE FEES button when Account Name is blank$")
public void tap_CALCULATE_FEES_button_when_Account_Name_is_blank() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.when_Account_Name_is_blank();
    
}

@Then("^Tap Verify Allowed Cahracter input on Account Name field$")
public void tap_Verify_Allowed_Cahracter_input_on_Account_Name_field() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.accountname_validation();

}

@Then("^Tap CALCULATE FEES button when Account Number is blank$")
public void tap_CALCULATE_FEES_button_when_Account_Number_is_blank() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.when_Account_Number_is_blank();
    
}

@Then("^Tap CALCULATE FEES button when Account Number is less than or greater than the required digits$")
public void tap_CALCULATE_FEES_button_when_Account_Number_is_less_than_or_greater_than_the_required_digits() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.account_number_validation_1();
	sendmoneyoption.account_number_validation_2();

}

@Then("^Tap CALCULATE FEES button when Amount is blank$")
public void tap_CALCULATE_FEES_button_when_Amount_is_blank() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.when_Amount_is_blank();
   
}

@Then("^Tap CALCULATE FEES button when Amount Entered is less than or greater than the required value$")
public void tap_CALCULATE_FEES_button_when_Amount_Entered_is_less_than_or_greater_than_the_required_value() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.Amount_validation_1();
	sendmoneyoption.Amount_validation_2();
}

@Then("^Tap Send Money when SALPAy balance is insufficient to send the Amount$")
public void tap_Send_Money_when_SALPAy_balance_is_insufficient_to_send_the_Amount() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.SALPAy_balance_insufficient();
}

@Then("^Change bank name when Send money Form is filled$")
public void change_bank_name_when_Send_money_Form_is_filled() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.change_bank1();
}

@Then("^Verify processing details$")
public void verify_processing_details() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.processing_detail();
	
}

@Then("^Update Account name when processing fee is already calculated$")
public void update_Account_name_when_processing_fee_is_already_calculated() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.update_Account_name();

}

@Then("^Update Account Number when processing fee is already calculated$")
public void update_Account_Number_when_processing_fee_is_already_calculated() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.update_Account_number();
    
}

@Then("^Update Amount when processing fee is already calculated$")
public void update_Amount_when_processing_fee_is_already_calculated() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.update_Amount();
    
}
@Then("^Send money verification page$")
public void send_money_verification_page() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.send_money_verification_page();
   
}

@Then("^Verify Back icon on the send money verification page$")
public void verify_Back_icon_on_the_send_money_verification_page() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.back_from_send_money_verification_page();
    
}

@Then("^Resubmit the Send Money form$")
public void resubmit_the_Send_Money_form() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.Resubmit_send_money_verification_page();
  
}

@Then("^Verify Cancel icon on the send money verification page$")
public void verify_Cancel_icon_on_the_send_money_verification_page() throws Throwable {
    
}

@Then("^Input incorrect verification code$")
public void input_incorrect_verification_code() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.Input_incorrect_verification_code();
    
}

@Then("^Verify Resend link text$")
public void verify_Resend_link_text() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.Resend_verification_code();
}

@Then("^Input the old verification code$")
public void input_the_old_verification_code() throws Throwable {
    
}

@Then("^Verify Transaction when account Balance is insufficient$")
public void verify_Transaction_when_account_Balance_is_insufficient() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.Enter_correct_OTP();
    
}

@Then("^Input correct verification code$")
public void input_correct_verification_code() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.Enter_correct_OTP();
}

@Then("^Return to home screen$")
public void return_to_home_screen() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.Return_to_home_screen();
    
}

@Then("^Verify Transaction History$")
public void verify_Transaction_History() throws Throwable {
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.verify_transaction_history();
  
}



   
   @Then("^Click on Send Money$")
   public void click_on_Send_Money() throws Throwable {
	   SendMoney sendmoney=new SendMoney();
	   sendmoney.sendmoney();
   }
   @Then("^calculate fees$")
   public void calculate_fees() throws Throwable {
     Calculatefees fees= new Calculatefees();
     fees.calculatefees();
   }
/*@AfterTest	   
    @Then("^Open App2$")
    public void open_App2() throws Throwable {
	
    }*/

}