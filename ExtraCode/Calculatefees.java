package com.salarium.android.testautomation.pageobjects;

import com.salarium.android.testautomation.driver.Salarium;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Calculatefees {

	AndroidDriver<AndroidElement> driver= Salarium.driver;
	public void calculatefees() throws InterruptedException{
		driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/submitButton']")));
		driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/submitButton']")).click();
		Thread.sleep(2000);
	}
}
