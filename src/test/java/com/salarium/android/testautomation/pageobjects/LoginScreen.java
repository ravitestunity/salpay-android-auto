package com.salarium.android.testautomation.pageobjects;

import com.salarium.android.testautomation.commonlibrary.*;
import com.salarium.android.testautomation.driver.AndroidDriverInit;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import java.io.IOException;


public class LoginScreen {

    private static Logger log;
    private static String message;
    private static AndroidDriver<AndroidElement> driver = AndroidDriverInit.driver;

    static {
        try {
            log = Utility.getTheLogger("LoginScreen");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void verifyUserOnLoginScreen()
    {
        if(Report.checkPreviousStepStatusNotFailNoRun())
        {
            StringBuffer stepLog = Report.getStepLog();
            Report.setCurrentStepStatus(Report.STATUS.IN_PROGRESS);
            log.info("verifyUserOnLoginScreen function called");
            String screenName = "LoginScreen";
            String  loginEditFld = "login-edit-field";
            WebElement element = AppiumCommLib.getWebElementElseLogError(screenName,loginEditFld);
            if(AppiumCommLib.verifyElmentVisibleHard(element)==AppiumCommLib.ASSERT_STATUS.FAIL)
            {
                Report.setCurrentStepStatus(Report.STATUS.FAIL);
                message = "Assertion failed while checking visibility of element -:"+loginEditFld+" on screen :-"+screenName;
                stepLog.append(message);
                log.error(message);
                Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            else
            {
                message = "Assertion PASSED while checking visibility of element -:"+loginEditFld+" on screen :-"+screenName;
                stepLog.append(message);
                log.info(message);
                Report.setCurrentStepStatus(Report.STATUS.PASS);
                Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            log.info("verifyUserOnLoginScreen function ended execution");
            Report.setStepLog(stepLog);
        }

    }

    public void doLogin(String userId,String password)
    {
        if(Report.checkPreviousStepStatusNotFailNoRun())
        {
            StringBuffer stepLog = Report.getStepLog();
            Report.setCurrentStepStatus(Report.STATUS.IN_PROGRESS);
            log.info("verifyUserOnLoginScreen function called");
            String screenName = "LoginScreen";
            String  loginEditFld = "login-edit-field";
            WebElement idELement = AppiumCommLib.getWebElementElseLogError(screenName,loginEditFld);
            String  pwdEditFld = "password-edit-field";
            WebElement pwdElement = AppiumCommLib.getWebElementElseLogError(screenName,pwdEditFld);
            String  loginBtn = "login-button";
            WebElement LognElement = AppiumCommLib.getWebElementElseLogError(screenName,loginBtn);
            if(idELement!=null && pwdElement !=null)
            {
                boolean editFiledDataEntered = AppiumCommLib.enterDataInFieldWeb(idELement,userId,loginEditFld,screenName,15);
                boolean pwdFiledDataEntered = AppiumCommLib.enterDataInFieldWeb(pwdElement,password,pwdEditFld,screenName,15);
                if(editFiledDataEntered&&pwdFiledDataEntered)
                {
                    message="Data could be sucessfully entered in fields-:"+loginEditFld+" and in -:"+pwdEditFld +" on screen: -"+screenName;
                    stepLog.append(message);
                    log.info(message);
                    Utility.takeScreenshot(driver,Report.getCurrentStepName());
                   boolean btnClicked= AppiumCommLib.clickOnWebElement(LognElement,loginBtn,screenName,15);
                   if(btnClicked)
                   {
                       message="Button could be clicked -:"+loginBtn+" on screen-:"+screenName;
                       stepLog.append(message);
                       log.info(message);
                       Report.setCurrentStepStatus(Report.STATUS.PASS);
                       Utility.takeScreenshot(driver,Report.getCurrentStepName());
                   }
                   else
                   {
                       Report.setCurrentStepStatus(Report.STATUS.FAIL);
                       message="Button could not be clicked -:"+loginBtn+" on screen-:"+screenName;
                       stepLog.append(message);
                       log.error(message);
                       Utility.takeScreenshot(driver,Report.getCurrentStepName());
                   }

                }else
                {
                    message="Data can not be entered in fields-:"+loginEditFld+" and in -:"+pwdEditFld +" on screen: -"+screenName;
                    stepLog.append(message);
                    log.error(message);
                    Report.setCurrentStepStatus(Report.STATUS.FAIL);
                    Utility.takeScreenshot(driver,Report.getCurrentStepName());
                }
            }
            else
            {
                message="Elements could not be located"+" on screen: -"+screenName;
                log.error(message);
                stepLog.append(message);
                Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            Report.setStepLog(stepLog);
        }

    }

    public void verifyUserLoggedInSucessfully()
    {
        StringBuffer stepLog = Report.getStepLog();
        if(Report.checkPreviousStepStatusNotFailNoRun())
        {
            Report.setCurrentStepStatus(Report.STATUS.IN_PROGRESS);
            log.info("verifyUserLoggedInSucessfully function called");
            String screenName = "HomeScreen";
            String  sendMoney = "send-money-button";
            WebElement element = AppiumCommLib.getWebElementElseLogError(screenName,sendMoney);
            if(element!=null)
            {
                if(AppiumCommLib.verifyElmentVisibleHard(element)==AppiumCommLib.ASSERT_STATUS.PASS)
                {
                    message = "Assertion Passed for Element Visibilit of element -:"+sendMoney+" on screen:-"+screenName;
                    stepLog.append(message);
                    log.info(message);
                    Report.setCurrentStepStatus(Report.STATUS.PASS);
                    Utility.takeScreenshot(driver,Report.getCurrentStepName());
                }
                else
                {
                    message = "Assertion FAILED for Element Visibilit of element -:"+sendMoney+" on screen:-"+screenName;
                    stepLog.append(message);
                    log.error(message);
                    Report.setCurrentStepStatus(Report.STATUS.FAIL);
                    Utility.takeScreenshot(driver,Report.getCurrentStepName());
                }
            }
            else
            {
                message = "Element -:"+sendMoney+" could not be found on screen:-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.FAIL);
                stepLog.append(message);
                log.error(message);
                Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            Report.setStepLog(stepLog);
            log.info("verifyUserLoggedInSucessfully function ended execution");
        }

    }

    public void setThePasscode()
    {
        if(Report.checkPreviousStepStatusNotFailNoRun())
        {
            StringBuffer stepLog = Report.getStepLog();
            Report.setCurrentStepStatus(Report.STATUS.IN_PROGRESS);
            log.info("setThePasscode function called");
            String screenName = "LoginScreen";
            String  passcodeFiled1 = "pass-code-filed-1";
            if(AppiumCommLib.checkIfWebElementExist(screenName,passcodeFiled1))
            {
                AndroidDriver<AndroidElement> driver =AndroidDriverInit.driver;
                WebElement passCodeFiledElm = AppiumCommLib.getWebElementElseLogError(screenName,passcodeFiled1);

                String  passcodeFiled2 = "pass-code-filed-2";
                WebElement passCodeFiledElm2 = AppiumCommLib.getWebElementElseLogError(screenName,passcodeFiled2);

                String  passcodeFiled3 = "pass-code-filed-3";
                WebElement passCodeFiledElm3 = AppiumCommLib.getWebElementElseLogError(screenName,passcodeFiled3);

                String  passcodeFiled4 = "pass-code-filed-4";
                WebElement passCodeFiledElm4 = AppiumCommLib.getWebElementElseLogError(screenName,passcodeFiled4);

                String  continueBtn = "continue-button";
                WebElement continueBtnElm = AppiumCommLib.getWebElementElseLogError(screenName,continueBtn);

                if(passCodeFiledElm!=null && passCodeFiledElm2 !=null && passCodeFiledElm3!=null && passCodeFiledElm4 !=null &&  continueBtnElm !=null )
                {
                    driver.getKeyboard().sendKeys("1234");
                    boolean btnClicked= AppiumCommLib.clickOnWebElement(continueBtnElm,continueBtn,screenName,15);
                    if(btnClicked)
                    {
                        message="Button could be clicked -:"+continueBtn+" on screen-:"+screenName;
                        stepLog.append(message);
                        log.info(message);
                        String okButton = "ok-button";
                        WebElement okButtonElm = AppiumCommLib.getWebElementElseLogError(screenName,okButton);
                        boolean okBtnClicked = AppiumCommLib.clickOnWebElement(okButtonElm,okButton,screenName,20);
                        if(okBtnClicked)
                        {
                            Report.setCurrentStepStatus(Report.STATUS.PASS);
                            message="Button could be clicked -:"+okButton+" on screen-:"+screenName;
                            stepLog.append(message);
                            log.info(message);
                            Utility.takeScreenshot(driver,Report.getCurrentStepName());
                        }
                        else
                        {
                            Report.setCurrentStepStatus(Report.STATUS.FAIL);
                            message="Button could NOT be clicked -:"+okButton+" on screen-:"+screenName;
                            stepLog.append(message);
                            log.error(message);
                            Utility.takeScreenshot(driver,Report.getCurrentStepName());
                        }
                    }
                    else
                    {
                        Report.setCurrentStepStatus(Report.STATUS.FAIL);
                        message="Button could NOT be clicked -:"+continueBtn+" on screen-:"+screenName;
                        stepLog.append(message);
                        log.error(message);
                        Utility.takeScreenshot(driver,Report.getCurrentStepName());
                    }
                }
                else
                {
                    message = "Elements could not be found on screen:-"+screenName;
                    Report.setCurrentStepStatus(Report.STATUS.FAIL);
                    stepLog.append(message);
                    log.error(message);
                    Utility.takeScreenshot(driver,Report.getCurrentStepName());
                }
            }
            else
            {
                message = "Passcode is not required to be filled or could not be able to login itself";
                Report.setCurrentStepStatus(Report.STATUS.PASS);
                stepLog.append(message);
                log.info(message);
                Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            Report.setStepLog(stepLog);
        }
        log.info("setThePasscode function call ended");
    }
}