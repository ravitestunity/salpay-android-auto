package com.salarium.android.testautomation.pageobjects;

import com.salarium.android.testautomation.commonlibrary.AppiumCommLib;
import com.salarium.android.testautomation.commonlibrary.Report;
import com.salarium.android.testautomation.commonlibrary.Utility;
import com.salarium.android.testautomation.driver.AndroidDriverInit;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

import java.io.IOException;

public class HomeScreen {
    private static Logger log;
    private static String message;
    private static AndroidDriver<AndroidElement> driver = AndroidDriverInit.driver;
    static {
        try {
            log = Utility.getTheLogger("LoginScreen");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void verifyUserOnHomePage()
    {
        if(Report.checkPreviousStepStatusNotFailNoRun())
        {
            StringBuffer stepLog = Report.getStepLog();
            Report.setCurrentStepStatus(Report.STATUS.IN_PROGRESS);
            log.info("verifyUserOnLoginPage function called");
            String screenName = "HomeScreen";
            String sendMoneyBtn = "send-money-button";
            WebElement element = AppiumCommLib.getWebElementElseLogError(screenName,sendMoneyBtn);
            if(AppiumCommLib.verifyElmentVisibleHard(element)==AppiumCommLib.ASSERT_STATUS.FAIL)
            {
                message = "Assertion failed while checking visibility of element -:"+sendMoneyBtn+" on screen :-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.FAIL);
                stepLog.append(message);
                log.error(message);
              //  Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            else
            {
                message = "Assertion PASSED while checking visibility of element -:"+sendMoneyBtn+" on screen :-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.PASS);
                stepLog.append(message);
                log.info(message);
              //  Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            log.info("verifyUserOnLoginPage function ended execution");
            Report.setStepLog(stepLog);
        }

    }

    public void verifyAccountDetailsPresent()
    {
        if(Report.checkPreviousStepStatusNotFailNoRun())
        {
            StringBuffer stepLog = Report.getStepLog();
            Report.setCurrentStepStatus(Report.STATUS.IN_PROGRESS);
            log.info("verifyAccountDetails function called");
            String screenName = "HomeScreen";
            String accountHead = "my-account-heading";
            WebElement element = AppiumCommLib.getWebElementElseLogError(screenName,accountHead);
            if(AppiumCommLib.verifyElmentVisibleHard(element)==AppiumCommLib.ASSERT_STATUS.FAIL)
            {
                message = "Assertion failed while checking visibility of element -:"+accountHead+" on screen :-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.FAIL);
                stepLog.append(message);
                log.error(message);
               // Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            else
            {
                message = "Assertion PASSED while checking visibility of element -:"+accountHead+" on screen :-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.PASS);
                stepLog.append(message);
                log.info(message);
               // Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            log.info("verifyAccountDetails function ended execution");
            Report.setStepLog(stepLog);
        }

    }

    public void verifyBrowMoneyPresent()
    {
        if(Report.checkPreviousStepStatusNotFailNoRun())
        {
            StringBuffer stepLog = Report.getStepLog();
            Report.setCurrentStepStatus(Report.STATUS.IN_PROGRESS);
            log.info("verifyBrowMoneyPresent function called");
            String screenName = "HomeScreen";
            String buyload = "buy-load-button";
            WebElement element = AppiumCommLib.getWebElementElseLogError(screenName,buyload);
            if(AppiumCommLib.verifyElmentVisibleHard(element)==AppiumCommLib.ASSERT_STATUS.PASS)
            {
                message = "Assertion Passed while checking visibility of element -:"+buyload+" on screen :-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.PASS);
                stepLog.append(message);
                log.info(message);
              //  Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            else
            {
                message = "Assertion failed while checking visibility of element -:"+buyload+" on screen :-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.FAIL);
                stepLog.append(message);
                log.error(message);
               // Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            log.info("verifyBrowMoneyPresent function ended execution");
            Report.setStepLog(stepLog);
        }

    }


    public void verifyPayBillOptionPresent()
    {
        if(Report.checkPreviousStepStatusNotFailNoRun())
        {
            StringBuffer stepLog = Report.getStepLog();
            Report.setCurrentStepStatus(Report.STATUS.IN_PROGRESS);
            log.info("verifyElementsOnHomePage function called");
            String screenName = "HomeScreen";
            String payBillBtn = "pay-bill-button";
            WebElement element = AppiumCommLib.getWebElementElseLogError(screenName,payBillBtn);
            if(AppiumCommLib.verifyElmentVisibleHard(element)==AppiumCommLib.ASSERT_STATUS.PASS)
            {
                message = "Assertion Passed while checking visibility of element -:"+payBillBtn+" on screen :-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.PASS);
                stepLog.append(message);
                log.info(message);
               // Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            else
            {
                message = "Assertion failed while checking visibility of element -:"+payBillBtn+" on screen :-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.FAIL);
                stepLog.append(message);
                log.error(message);
                //Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            log.info("verifyPayBillOptionPresent function ended execution");
            Report.setStepLog(stepLog);

        }

    }

    public void verifyBuyLoadOptionPresent()
    {
        if(Report.checkPreviousStepStatusNotFailNoRun())
        {
            StringBuffer stepLog = Report.getStepLog();
            Report.setCurrentStepStatus(Report.STATUS.IN_PROGRESS);
            log.info("verifyElementsOnHomePage function called");
            String screenName = "HomeScreen";
            String buyLoadOptn = "buy-load-button";
            WebElement element = AppiumCommLib.getWebElementElseLogError(screenName,buyLoadOptn);
            if(AppiumCommLib.verifyElmentVisibleHard(element)==AppiumCommLib.ASSERT_STATUS.PASS)
            {
                message = "Assertion Passed while checking visibility of element -:"+buyLoadOptn+" on screen :-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.PASS);
                stepLog.append(message);
                log.info(message);
                //Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            else
            {
                message = "Assertion failed while checking visibility of element -:"+buyLoadOptn+" on screen :-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.FAIL);
                stepLog.append(message);
                log.error(message);
              //  Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            log.info("verifyPayBillOptionPresent function ended execution");
            Report.setStepLog(stepLog);
        }

    }

    public void verifyFlashLoanOptionPresent()
    {
        if(Report.checkPreviousStepStatusNotFailNoRun())
        {
            StringBuffer stepLog = Report.getStepLog();
            Report.setCurrentStepStatus(Report.STATUS.IN_PROGRESS);
            log.info("verifyFlashLoanOptionPresent function called");
            String screenName = "HomeScreen";
            String elementName = "flash-loan-option";
            WebElement element = AppiumCommLib.getWebElementElseLogError(screenName,elementName);
            if(AppiumCommLib.verifyElmentVisibleHard(element)==AppiumCommLib.ASSERT_STATUS.PASS)
            {
                message = "Assertion Passed while checking visibility of element -:"+elementName+" on screen :-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.PASS);
                stepLog.append(message);
                log.info(message);
              //  Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            else
            {
                message = "Assertion failed while checking visibility of element -:"+elementName+" on screen :-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.FAIL);
                stepLog.append(message);
                log.error(message);
               // Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            log.info("verifyFlashLoanOptionPresent function ended execution");
            Report.setStepLog(stepLog);
        }

    }

    public void verifyEasyLoanOptionPresent()
    {
        if(Report.checkPreviousStepStatusNotFailNoRun())
        {
            StringBuffer stepLog = Report.getStepLog();
            Report.setCurrentStepStatus(Report.STATUS.IN_PROGRESS);
            log.info("verifyEasyLoanOptionPresent function called");
            String screenName = "HomeScreen";
            String elementName = "easy-loan-option";
            WebElement element = AppiumCommLib.getWebElementElseLogError(screenName,elementName);
            if(AppiumCommLib.verifyElmentVisibleHard(element)==AppiumCommLib.ASSERT_STATUS.PASS)
            {
                message = "Assertion Passed while checking visibility of element -:"+elementName+" on screen :-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.PASS);
                stepLog.append(message);
                log.info(message);
               // Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            else
            {
                message = "Assertion failed while checking visibility of element -:"+elementName+" on screen :-"+screenName;
                Report.setCurrentStepStatus(Report.STATUS.FAIL);
                stepLog.append(message);
                log.error(message);
               // Utility.takeScreenshot(driver,Report.getCurrentStepName());
            }
            log.info("verifyEasyLoanOptionPresent function ended execution");
            Report.setStepLog(stepLog);
        }
    }
}
