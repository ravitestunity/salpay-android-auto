package com.salarium.android.testautomation.commonlibrary;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.log4j.Logger;

public class ReportToHTML {

    public static String reportFileName = null;
    public static boolean reportFileExist = false;

    private static Logger Log;
    static
    {
        try {
            Log = Utility.getTheLogger("ReportToHTML");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    protected static boolean createHTMLReportFile()
    {
        boolean propertyFileRead;
        String testLevel ="";
        String reportFilePath = "";
        String whichReportPath="";
        ReadPropertyValues readPropertyValues = new ReadPropertyValues();
        try {
            testLevel = readPropertyValues.getPropValues("TestLevel");
            propertyFileRead = true;
        } catch (IOException e) {
            e.printStackTrace();
            Log.error("Configuration file could not be read",e);
            propertyFileRead= false;
        }
        if(propertyFileRead)
        {
            if(testLevel.equalsIgnoreCase("smoke"))
            {
                whichReportPath = "smokeReportFilePath";
            }
            else if(testLevel.equalsIgnoreCase("functional"))
            {
                whichReportPath = "functionalReportFilePath";
            }
            else if(testLevel.equalsIgnoreCase("system"))
            {
                whichReportPath = "systemReportFilePath";
            }
            else if(testLevel.equalsIgnoreCase("regression"))
            {
                whichReportPath = "regressionReportFilePath";
            }
            try {
                reportFilePath = readPropertyValues.getPropValues(whichReportPath);
                propertyFileRead = true;
            } catch (IOException e) {
                e.printStackTrace();
                Log.error("Configuration file could not be read",e);
                propertyFileRead= false;
            }
            if(propertyFileRead)
            {
                boolean propertyRead = false;
                String fileName = "SALPAY-ANDROIDAPP-AUTOTEST-EXECUTION-";
                String ext =".html";
                fileName = Utility.getFileNameWithTimeStamp(fileName,ext);
                fileName = reportFilePath+fileName;
                File reportFile = new File(fileName);
                try {
                    propertyRead = reportFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.error("Report File Could not be created",e);
                    propertyRead = false;
                }
                if(propertyRead)
                {

                    reportFileName = fileName;
                    reportFileExist = true;
                    return true;
                }
                else
                {

                    reportFileName = null;
                    reportFileExist = false;
                    return false;
                }
            }
        }
        return false;
    }
    protected static void createHTMLHeader()
    {
        ArrayList<String> htmlHeader = new ArrayList<>();
        htmlHeader.add("<!DOCTYPE html>");
        htmlHeader.add("<html>");
        htmlHeader.add("<head>");
        htmlHeader.add("<h1 align='center'>Automated Test Execution Report for SAL PAY APP - "+new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date())+"</h1>");
        htmlHeader.add("<h2  align='center' style='color:blue;'>Feature - :"+Report.getCurrentFeatureName()+"</h2>");
        htmlHeader.add("<style>");
        htmlHeader.add("table {");
        htmlHeader.add("    font-family: arial, sans-serif;");
        htmlHeader.add("    border-collapse: collapse;");
        htmlHeader.add("    width: 100%;");
        htmlHeader.add("}");
        htmlHeader.add("td, th {");
        htmlHeader.add("    border:  2px solid black;");
        htmlHeader.add("    text-align: left;");
        htmlHeader.add("    padding: 8px;");
        htmlHeader.add("}");
        htmlHeader.add("tr:nth-child(even) {");
        htmlHeader.add("    background-color: #dddddd;");
        htmlHeader.add("}");
        htmlHeader.add("</style>");
        htmlHeader.add("</head>");
        htmlHeader.add("<body bgcolor=\"#F0F8FF\">");
        htmlHeader.add("<table>");
        htmlHeader.add("<tr>");
        htmlHeader.add("	<th width=\"10%\">S.NO</th>");
        htmlHeader.add("    <th width=\"30%\">SCENARIO</th>");
        htmlHeader.add("    <th width=\"10%\">SCENARIO STATUS</th>");
        htmlHeader.add("    <th width=\"30%\">STEPS</th>");
        htmlHeader.add("    <th width=\"10%\">STEP STATUS</th>");
        htmlHeader.add("    <th width=\"10%\">STEP LOG</th>");
        htmlHeader.add("</tr>");
        writeToHTMLFile(htmlHeader);
    }

    protected static void createHTMLBody()
    {
        ArrayList<String> htmlBody = new ArrayList<>();
        ArrayList<Scenario> scenarioArrayList = Report.getAllScenariosAsListInOrder();
        for (int sceNo=0;sceNo<scenarioArrayList.size();sceNo++) {
            Scenario currentScenario = scenarioArrayList.get(sceNo);

            ArrayList<Step> allSteps = Report.getAllStepsInScenarioOrdered(currentScenario);
            int noSteps = allSteps.size();

            String currentHTMLStatment = "";

            htmlBody.add("<tr>");
            htmlBody.add("<td rowspan='"+noSteps+"'align='center'>"+(sceNo+1)+"</td>");
            htmlBody.add("<td rowspan='"+noSteps+"'align='center'>"+currentScenario.getScenarioName()+"</td>");
            currentHTMLStatment = getScenarioHTMLStatusStatment(currentScenario,noSteps);
            htmlBody.add(currentHTMLStatment);
            htmlBody.add("<td>"+allSteps.get(0).getStepName()+"</td>");
            currentHTMLStatment = getStepHTMLStatusStatment(allSteps.get(0));
            htmlBody.add(currentHTMLStatment);
            htmlBody.add("<td>"+allSteps.get(0).getStepLog()+"</td>");
            htmlBody.add("</tr>");

            for(int j=1;j<allSteps.size();j++)
            {
                String currentStmnt ="";
                htmlBody.add("<tr>");
                htmlBody.add("<td>"+allSteps.get(j).getStepName()+"</td>");
                currentStmnt = getStepHTMLStatusStatment(allSteps.get(j));
                htmlBody.add(currentStmnt);
                htmlBody.add("<td>"+allSteps.get(j).getStepLog()+"</td>");
                htmlBody.add("</tr>");
            }
        }
        writeToHTMLFile(htmlBody);
    }

    public static String getScenarioHTMLStatusStatment(Scenario currentScenario,int noSteps)
    {
        String currentHTMLStatment="";
        if(currentScenario.getScenarioStatus()==Report.STATUS.PASS)
        {
            currentHTMLStatment = "<td rowspan='"+noSteps+"' bgcolor='#00FF00' align='center'>PASS</td>";
        }
        if(currentScenario.getScenarioStatus()==Report.STATUS.FAIL)
        {

            currentHTMLStatment ="<td rowspan='"+noSteps+"' bgcolor='#FF0000' align='center'>FAIL</td>";
        }
        if(currentScenario.getScenarioStatus()==Report.STATUS.IN_PROGRESS)
        {
            currentHTMLStatment ="<td rowspan='"+noSteps+"' bgcolor='#FFA500' align='center'>IN-PROGRESS</td>";
        }
        if(currentScenario.getScenarioStatus()==Report.STATUS.NOT_RUN)
        {
            currentHTMLStatment ="<td rowspan='"+noSteps+"' bgcolor='#FF7F50' align='center'>NO RUN</td>";
        }
        return currentHTMLStatment;
    }

    public static String getStepHTMLStatusStatment(Step step)
    {
        String currentHTMLStatment="";
        if(step.getStepStatus()==Report.STATUS.PASS)
        {
            currentHTMLStatment = "<td bgcolor='#00FF00' align='center'>PASS</td>";
        }
        if(step.getStepStatus()==Report.STATUS.FAIL)
        {
            currentHTMLStatment ="<td bgcolor='#FF0000' align='center'>FAIL</td>";
        }
        if(step.getStepStatus()==Report.STATUS.IN_PROGRESS)
        {
            currentHTMLStatment = "<td bgcolor='#FFA500' align='center'>IN-PROGRESS</td>";
        }
        if(step.getStepStatus()==Report.STATUS.NOT_RUN)
        {
            currentHTMLStatment ="<td bgcolor='#FF7F50' align='center'>NO RUN</td>";
        }
        return currentHTMLStatment;
    }

    protected  static void createHTMLFooter()
    {
        ArrayList<String> footerHTML = new ArrayList<>();
        footerHTML.add("</table>");
        footerHTML.add("</body>");
        footerHTML.add("</html>");
        writeToHTMLFile(footerHTML);
    }

    private static void writeToHTMLFile(ArrayList<String> thingsToWrite)
    {

        BufferedWriter bw = null;
        try
        {
            File file = new File(reportFileName);
            FileWriter fw = new FileWriter(file.getAbsolutePath(), true);
            bw = new BufferedWriter(fw);
            for(String item:thingsToWrite)
            {
                bw.write(item);
                bw.newLine();
                bw.flush();
            }
        }
        catch (IOException e) {
            Log.error("Report File to write Could not be Opened : "+reportFileName,e);
        }
        finally {                       // always close the file
            if (bw != null) try {
                bw.close();
            } catch (IOException ioe) {
                Log.error("Report File Could not be closed : "+reportFileName,ioe);
            }
        } // end try/catch/finally

}

    public static boolean checkIfReportFileisEmpty()
    {

        File file =null;
        if(reportFileName!=null)
        {
            try
            {
                file = new File(reportFileName);
                return (file.length() == 0);
            }
            catch (NullPointerException npe)
            {
                npe.printStackTrace();
            }
        }
        return false;
    }

    public static boolean deleteAndCreateNewFileAgain()
    {
        File file;
        if (reportFileName != null)
        {
            try
            {
                file = new File(reportFileName);
                if(file.exists()){
                    if(file.delete())
                    {
                        try {
                            return file.createNewFile();
                        } catch (IOException e) {
                            e.printStackTrace();
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    return true;
                }
            }
            catch (NullPointerException npe)
            {
                npe.printStackTrace();
            }

        }
        else
        {
            return false;
        }
        return false;
    }
}
