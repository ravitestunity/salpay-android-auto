package com.salarium.android.testautomation.commonlibrary;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PrepearTestData {
    public static Map<String,String> getTheTestData() throws IOException {
        ReadPropertyValues readPropertyValues = new ReadPropertyValues();
        String envnName = readPropertyValues.getPropValues("ExecutionEnvironment");
        String testLevel = readPropertyValues.getPropValues("TestLevel");
        String FilePath = "";
        Map<String, String> mapToRet = new HashMap<String, String>();
        if(envnName.equalsIgnoreCase("staging"))
        {
            if(testLevel.equalsIgnoreCase("smoke"))
            {
                FilePath = readPropertyValues.getPropValues("SmokeTestTestDataFile");

            }
        }
        mapToRet=ReadJsonFile.readJsonFileReturnMap(FilePath);
        return mapToRet;
    }
}
