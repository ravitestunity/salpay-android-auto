package com.salarium.android.testautomation.commonlibrary;

import cucumber.api.Scenario;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

public class Utility {

	final static Logger logger = Logger.getLogger(Utility.class.getName());
	static Logger log;
    static {
        try {
            log = Utility.getTheLogger("Utility");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getFullPath(String halfPath)
    {
        halfPath = "/"+halfPath;
    	String currentDir = System.getProperty("user.dir");
    	currentDir=currentDir.replace("\\", "\\\\");
    	return  currentDir+halfPath;
    }

    public static void captureScreenShot(WebDriver driver,String stepName) throws IOException
    {
    	// Take screenshot and store as a file format
    	ReadPropertyValues rpDrv = new ReadPropertyValues();
    	String scrnShotFldrPath = rpDrv.getPropValues("ScreenshotFolderPath");
    	scrnShotFldrPath = getFullPath(scrnShotFldrPath);
    	 File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);           
    	try 
    	{
    	// now copy the  screenshot to desired location using copyFile method
    	 
    	FileUtils.copyFile(src, new File(scrnShotFldrPath+stepName+System.currentTimeMillis()+".png")); 
    	} 
    	catch (IOException e)
    	{
    	  logger.error(e.getMessage()) ;
    	}
    }

    public static void takeScreenshot( AndroidDriver<AndroidElement> driver,String stepName)
    {
        String message;
        try {
            message = "Screen Shot Captured";
            Utility.captureScreenShot(driver,stepName);
            log.info(message);
        } catch (IOException e) {
            message = "Screen Shot could not be Captured";
            e.printStackTrace();
            log.error(message,e);
        }
    }

    public static void captureScreenShot(AndroidDriver<AndroidElement> driver,String stepName) throws IOException
    {
        // Take screenshot and store as a file format
        ReadPropertyValues rpDrv = new ReadPropertyValues();
        String scrnShotFldrPath="";
        String testLevel = rpDrv.getPropValues("TestLevel");
        if(testLevel.equalsIgnoreCase("smoke"))
        {
            scrnShotFldrPath = rpDrv.getPropValues("ScreenshotFolderPathSmoke");
        }
        scrnShotFldrPath = getFullPath(scrnShotFldrPath);
        File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try
        {
            FileUtils.copyFile(src, new File(scrnShotFldrPath+stepName+System.currentTimeMillis()+".jpeg"));
        }
        catch (IOException e)
        {
            logger.error(e.getMessage()) ;
        }
    }




	public static String randomString( int len ){
        final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder( len );
		for( int i = 0; i < len; i++ )
			sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		return sb.toString();
	}

    public static String removeLast(String s,int start, int end) {
        if (s!=null && s!="" && s.length()>0) {
            s = s.substring(start,end);
        }
        return s;
    }

    public static Logger getTheLogger(String className) throws IOException {
        InputStream inputStream;
        System.setProperty("testexecution.log",new File("log/test-execution.log").getAbsolutePath());
        Properties props = new Properties();
        inputStream = new FileInputStream(new File("src/test/java/com/salarium/android/testautomation/configurations/log4j.properties").getAbsolutePath());
        props.load(inputStream);
        PropertyConfigurator.configure(props);
        return Logger.getLogger(className);
    }

    public static void setFeatureNameScenarioName(Scenario scenario) {
        String featureName = "Feature ";
        String rawFeatureName = scenario.getId().split(";")[0].replace("-"," ");
        featureName = featureName + rawFeatureName.substring(0, 1).toUpperCase() + rawFeatureName.substring(1);
        if(Report.getAllScenarios().isEmpty())
        {
            Report.setFeatureName(featureName);
            Report.addScenario(scenario.getName(),scenario.getId(),Report.STATUS.NOT_RUN);
        }
        else {
            if(!Report.ifSceanrioIdExist(scenario.getId()))
            {
                Report.addScenario(scenario.getName(),scenario.getId(),Report.STATUS.NOT_RUN);
            }
        }

    }


    public static String getFileNameWithTimeStamp(String fileName,String extention)
    {
        Date date = new Date(); // your date
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        int second = cal.get(Calendar.SECOND);
        String rawfileName = String.valueOf(year)+"-"+String.valueOf(month+1)+"-"+String.valueOf(day)+"-"+String.valueOf(hour);
        rawfileName = rawfileName+"-"+ String.valueOf(minute)+"-"+String.valueOf(second);
        fileName = fileName+"-"+rawfileName+extention;
        return fileName;
    }
}
