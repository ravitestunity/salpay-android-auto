package com.salarium.android.testautomation.commonlibrary;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

public class Report {
    private static String featureName;
    private static Stack<Scenario> scenarios = new Stack<>();
    public enum STATUS
    {
        PASS,
        FAIL,
        NOT_RUN,
        IN_PROGRESS
    }

    static Logger log;
    {
        try {
            log = Utility.getTheLogger("Report");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void addScenario(String senarioName,String scenarioId, STATUS scenarioStatus)
    {
        Scenario scenario = new Scenario();
        scenario.setScenarioName(senarioName);
        scenario.setScenarioStatus(scenarioStatus);
        scenario.setScenarioId(scenarioId);
        scenarios.push(scenario);
    }

    public static void addStepToScenaio(String stepName, STATUS stepStatus)
    {
        Scenario currentScenario = scenarios.peek();
        Step step = new Step();
        StringBuffer stringBuffer=new StringBuffer("");
        step.setStepName(stepName);
        step.setStepStatus(stepStatus);
        step.setStepLog(stringBuffer);
        currentScenario.addStepInCurrentScenario(step);
    }

    public static Scenario getCurrentScenario()
    {
        return scenarios.peek();
    }

    public static Step getCurrentStep()
    {
        Scenario currentScenario = scenarios.peek();
        return currentScenario.getCurrentStep();
    }

    public static String getCurrentStepName()
    {
        Scenario currentScenario = scenarios.peek();
        Step step =  currentScenario.getCurrentStep();
        return step.getStepName();
    }

    public static STATUS getCurrentScenarioStatus()
    {
        Scenario currentScenario =  scenarios.peek();
        return currentScenario.getScenarioStatus();
    }

    public static Stack<Scenario> getAllScenarios() {
        return scenarios;
    }

    public static ArrayList<Step> getAllStepsInScenarioOrdered(Scenario scenario) {
        ArrayList<Step> stepArrayList = new ArrayList<>(scenario.getAllStepsInScenario());
        return stepArrayList;
    }

    public static ArrayList<Scenario> getAllScenariosAsListInOrder() {
      ArrayList<Scenario> scenarioArrayList =   new ArrayList<>(scenarios);
      return scenarioArrayList;
    }

    public static boolean ifSceanrioIdExist(String sceanrioToLookfor)
    {
        if(!scenarios.isEmpty())
        {
            ArrayList<Scenario> scenarioArrayList = new ArrayList<>(scenarios);
            Iterator<Scenario> iterator = scenarioArrayList.iterator();
            while(iterator.hasNext())
            {
                if(iterator.next().getScenarioId().equalsIgnoreCase(sceanrioToLookfor))
                    return true;
            }

        }
        return false;
    }

    public static String getCurrentFeatureName() {
        return featureName;
    }

    public static ArrayList<String> getStepNames() {
        ArrayList<String> stepNamesFromCurrentSceanrio = new ArrayList<>();
        if(!scenarios.isEmpty())
        {
            Scenario currentScenario = scenarios.peek();
            Stack<Step> allSteps = currentScenario.getAllStepsInScenario();
            ArrayList<Step> stepArrayList = new ArrayList<>(allSteps);
            Iterator<Step> iterator = stepArrayList.iterator();
            while(iterator.hasNext())
            {
                stepNamesFromCurrentSceanrio.add(iterator.next().getStepName());
            }

        }
        return stepNamesFromCurrentSceanrio;
    }


    public static void setFeatureName(String currentfeatureName) {
        featureName=currentfeatureName;
    }


    public static void setCurrentSceanrioStatus(STATUS currentSceanrioStatus) {
        Scenario currentScenario = scenarios.peek();
        currentScenario.setScenarioStatus(currentSceanrioStatus);
    }

    public static ArrayList<STATUS> getAllStepStatus() {
        ArrayList<STATUS> stepStatusCurrentSceanrio = new ArrayList<>();
        if(!scenarios.isEmpty())
        {
            Scenario currentScenario = scenarios.peek();
            Stack<Step> allSteps = currentScenario.getAllStepsInScenario();
            ArrayList<Step> stepArrayList = new ArrayList<>(allSteps);
            Iterator<Step> iterator = stepArrayList.iterator();
            while(iterator.hasNext())
            {
                stepStatusCurrentSceanrio.add(iterator.next().getStepStatus());
            }

        }
        return stepStatusCurrentSceanrio;
    }

    public static STATUS getScenarioStatusAtEnd()
    {
        boolean isPass = false;
        ArrayList<STATUS> allStepStatus = getAllStepStatus();
        if(!allStepStatus.isEmpty())
        {
            Iterator<STATUS> iterator = allStepStatus.iterator();
            while(iterator.hasNext())
            {
                STATUS curretStatus = iterator.next();
                if(curretStatus==STATUS.FAIL)
                {
                    return STATUS.FAIL;
                }
                else if(curretStatus==STATUS.PASS)
                {
                    isPass=true;
                }
            }
        }
        if(isAllStepsInScenarioNOTRUN())
        {
            return STATUS.NOT_RUN;
        }
        else if(isPass)
        {
            return STATUS.PASS;
        }
        else
        {
            return STATUS.FAIL;
        }

    }

    public static boolean isAllStepsInScenarioNOTRUN()
    {
        boolean stepNotRun = false;
        ArrayList<STATUS> allStepStatus = getAllStepStatus();
        if(!allStepStatus.isEmpty())
        {
            Iterator<STATUS> iterator = allStepStatus.iterator();
            while(iterator.hasNext())
            {
                if(iterator.next()==STATUS.NOT_RUN )
                {
                    stepNotRun = true;
                }
                else
                {
                    stepNotRun = false;
                    break;
                }
            }
        }
        if(stepNotRun)
            return true;
        else
            return false;
    }

    public static void setCurrentStepStatus(STATUS currentStepStatus) {
        Scenario currentScenario = scenarios.peek();
        Step currentStep = currentScenario.getCurrentStep();
        currentStep.setStepStatus(currentStepStatus);
     }

    public static STATUS getCurrentStepStatus() {
        Scenario currentScenario = scenarios.peek();
        Step currentStep = currentScenario.getCurrentStep();
        return currentStep.getStepStatus();
    }

    public static boolean checkPreviousStepStatusNotFailNoRun() {
        Scenario currentScenario = scenarios.peek();
        Stack<Step> allSteps = currentScenario.getAllStepsInScenario();
        ArrayList<Step> stepArrayList = new ArrayList<>(allSteps);
        if(stepArrayList.size()>=2)
        {
            STATUS prevStepStatus  = stepArrayList.get(stepArrayList.size()-2).getStepStatus();
            if(prevStepStatus==STATUS.PASS)
                return true;
            else
                return false;
        }
        else {
            return true;
        }
    }

    public static void setCurrentStepStatusAtStepEND() {
        STATUS currentStepStatus = getCurrentStepStatus();
        if(currentStepStatus != STATUS.FAIL && currentStepStatus!=STATUS.NOT_RUN)
           setCurrentStepStatus(STATUS.PASS);
    }


    public static StringBuffer getStepLog() {
        Scenario currentScenario = scenarios.peek();
        Step currentStep = currentScenario.getCurrentStep();
        return currentStep.getStepLog();
    }

    public static void setStepLog(StringBuffer stringBuffer) {
        Scenario currentScenario = scenarios.peek();
        Step currentStep = currentScenario.getCurrentStep();
        StringBuffer stepLog = currentStep.getStepLog();
        currentStep.setStepLog(stepLog);
    }

    public static void setCurrentSceanrioStatusAtStepEnd(boolean lastStep) {
        if(!lastStep)
        {
            if(Report.getCurrentStepStatus()==Report.STATUS.FAIL)
            {
                Report.setCurrentSceanrioStatus(Report.STATUS.FAIL);
            }
            else
            {
                Report.setCurrentSceanrioStatus(Report.STATUS.IN_PROGRESS);
            }
        }
        else
        {
            Report.setCurrentSceanrioStatus(Report.getScenarioStatusAtEnd());
        }

    }



    public static void createCustomHTMLReport()
    {

        if(ReportToHTML.reportFileExist==false&&ReportToHTML.reportFileName==null)
        {
            ReportToHTML.createHTMLReportFile();
        }
        if(!ReportToHTML.checkIfReportFileisEmpty())
        {
            if(ReportToHTML.deleteAndCreateNewFileAgain())
            {
                if(ReportToHTML.reportFileExist)
                {
                    ReportToHTML.createHTMLHeader();
                    ReportToHTML.createHTMLBody();
                    ReportToHTML.createHTMLFooter();
                }
                else
                {
                    log.error("Report HTML File Could not be created");
                }
            }
        }
        else
        {
            if(ReportToHTML.reportFileExist)
            {
                ReportToHTML.createHTMLHeader();
                ReportToHTML.createHTMLBody();
                ReportToHTML.createHTMLFooter();
            }
            else
            {
               // log.error("Report HTML File Could not be created");
            }
        }

    }

    public static void setStepScenarioStatusWriteToHTMLAtStepEnd(boolean lastStep)
    {
        Report.setCurrentStepStatusAtStepEND();
        Report.setCurrentSceanrioStatusAtStepEnd(lastStep);
        Report.createCustomHTMLReport();
    }
}

class Scenario
{
    private String scenarioName;
    private String scenarioId;
    private Report.STATUS scenarioStatus;
    private Stack<Step> allStepsInScenario = new Stack<>();

    public String getScenarioName() {
        return scenarioName;
    }

    public void setScenarioName(String scenarioName) {
        this.scenarioName = scenarioName;
    }

    public String getScenarioId() {
        return scenarioId;
    }

    public void setScenarioId(String scenarioId) {
        this.scenarioId = scenarioId;
    }

    public Stack<Step> getStepsInCurrentScenario() {
        return allStepsInScenario;
    }

    public void addAllStepsToCurrentScenario(Stack<Step> stepsInCurrentScenario) {
        this.allStepsInScenario = stepsInCurrentScenario;
    }

    public void addStepInCurrentScenario(Step currentStep) {
        allStepsInScenario.push(currentStep);
    }

    public Step getCurrentStep() {
        return  allStepsInScenario.peek();
    }

    public Report.STATUS getScenarioStatus() {
        return scenarioStatus;
    }

    public void setScenarioStatus(Report.STATUS scenarioStatus) {
        this.scenarioStatus = scenarioStatus;
    }

    public Stack<Step> getAllStepsInScenario()
    {
        return allStepsInScenario;
    }
}

class Step
{
    private String stepName;
    private Report.STATUS stepStatus;
    private StringBuffer stepLog;

    public String getStepName() {
        return stepName;
    }

    public StringBuffer getStepLog() {
        return stepLog;
    }

    public void setStepLog(StringBuffer stepLog) {
        this.stepLog = stepLog;
    }

    public void setStepName(String stepName) {

        this.stepName = stepName;
    }

    public Report.STATUS getStepStatus() {
        return stepStatus;
    }

    public void setStepStatus(Report.STATUS stepStatus) {
        this.stepStatus = stepStatus;
    }
}