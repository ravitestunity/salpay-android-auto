package com.salarium.android.testautomation.commonlibrary;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class ReadJsonFile {
    public static Map<String,String> readJsonFileReturnMap(String filePath) {
        JSONParser parser = new JSONParser();
        Map<String, String> map= new HashMap<String, String>();
        JSONObject jsonObject = null;
        try {
             File file = new File(filePath);
             String absolutePath = file.getAbsolutePath();
             Object obj = parser.parse(new FileReader(absolutePath));
             jsonObject = (JSONObject) obj;
             map = convertToMap(jsonObject);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return map;
    }

    private static Map<String, String> convertToMap(JSONObject jsonobj) {
        Map<String, String> map = new HashMap<String, String>();
        Set<String> keys =(Set<String>) jsonobj.keySet();
        Iterator iter = keys.iterator();
        while (iter.hasNext()) {
            String key =(String) iter.next();
            String value = (String) jsonobj.get(key);
            map.put(key, value);
        }
        return map;
    }

    public static Map<String,ArrayList<String>> readJsonFileReturnMapArray(String filePath) {
        JSONParser parser = new JSONParser();
        Map<String, ArrayList<String>> map= new HashMap<String, ArrayList<String>>();
        JSONObject jsonObject = null;
        try {
            File file = new File(filePath);
            String absolutePath = file.getAbsolutePath();
            Object obj = parser.parse(new FileReader(absolutePath));
            jsonObject = (JSONObject) obj;
            map = convertToMapArray(jsonObject);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return map;
    }

    private static Map<String, ArrayList<String>> convertToMapArray(JSONObject jsonobj) {
        Map<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>();
        Set<String> keys =(Set<String>) jsonobj.keySet();
        Iterator iter = keys.iterator();
        while (iter.hasNext()) {
            String key =(String) iter.next();
            ArrayList<String> value = (ArrayList<String>) jsonobj.get(key);
            map.put(key, value);
        }
        return map;
    }
}
