package com.salarium.android.testautomation.commonlibrary;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.salarium.android.testautomation.commonlibrary.ReadJsonFile.*;

public class PrepearObjectRepo {
    static Logger log;
    static
    {

        try {
              log = Utility.getTheLogger("PrepearObjectRepo");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static String getTheLocatorStrategyNameValue(String screenName, String elementName, String requiredItem) throws IOException {
        Map<String, ArrayList<String >> objectRepoMap = new HashMap<>();
        objectRepoMap = getTheMapOfObjectRepoElemtns(screenName);
        ArrayList<String> arrayObjRep = objectRepoMap.get(elementName);
        if(requiredItem.equalsIgnoreCase("LocatorStartegy"))
        {
            return arrayObjRep.get(0);
        }
        else if(requiredItem.equalsIgnoreCase("LocatorValue"))
        {
            return arrayObjRep.get(1);
        }
        else
        {
            return null;
        }
    }
    private static Map<String, ArrayList<String >> getTheMapOfObjectRepoElemtns(String screenName) throws IOException {
        ReadPropertyValues props = new ReadPropertyValues();
        String pathORFile = props.getPropValues(screenName);
        File file = new File(pathORFile);
        String completePathObjRepFile =file.getAbsolutePath();
        Map<String, ArrayList<String >> objectRepoMap = new HashMap<>();
        objectRepoMap=readJsonFileReturnMapArray(completePathObjRepFile);
        return objectRepoMap;
    }

    public static String getTheLocatorType(String screenName,String fieldName)
    {
        try {
            return PrepearObjectRepo.getTheLocatorStrategyNameValue(screenName,fieldName,"LocatorStartegy");
        } catch (IOException e) {
            log.error("Configuration file could not be read",e);
            return null;
        }
    }

    public static String getTheLocatorValue(String screenName,String fieldName)
    {
        try {
            return PrepearObjectRepo.getTheLocatorStrategyNameValue(screenName,fieldName,"LocatorValue");
        } catch (IOException e) {
            log.error("Configuration file could not be read",e);
            return null;
        }
    }
}
