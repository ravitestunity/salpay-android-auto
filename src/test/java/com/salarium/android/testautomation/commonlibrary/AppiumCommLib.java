package com.salarium.android.testautomation.commonlibrary;

import com.salarium.android.testautomation.driver.AndroidDriverInit;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.MobileBy;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


public class AppiumCommLib {
    /*Common Code for:
1. Find a Element with error handling - Return Element
2. Click a element with error handling - Return if it could
3. Enter a data in edit field with error handling - Return Yes not if Could
4. To see if element exist - Return yes no if
5. Verify an element exist -Assertion (Soft)
6. Verify Text of an element - Assertion (Soft)
7. Verify a element must exist - Assertion (Hard)

*/
    static AndroidDriver driver = AndroidDriverInit.driver;
    static Logger Log;
    static StringBuffer stepLog;

    static {
        try {
            Log = Utility.getTheLogger("AppiumCommLib");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static MobileElement locateMobileElementByAccClass(String locatorStrategy, String locator)
    {
         MobileElement mobileElement=null;
         try
         {
             if(locatorStrategy.equalsIgnoreCase("AccessibilityId"))
             {
                 mobileElement=(MobileElement) driver.findElementByAccessibilityId(locator);
             }
             else
             {
                 mobileElement = (MobileElement) driver.findElementByClassName(locator);
             }
             return mobileElement;
         }
         catch (org.openqa.selenium.NoSuchElementException noSuchElmExp)
         {
             Log.error(noSuchElmExp.getMessage());
             return null;
         }

    }

    public static boolean clickOnMobileElement(MobileElement mobileElement)
    {

        WebDriverWait wait = new WebDriverWait(driver,15);
        try
        {
            wait.until(ExpectedConditions.elementToBeClickable(mobileElement));
        }
        catch (TimeoutException timeOut)
        {
            Log.error(timeOut.getMessage());
            return false;
        }
        mobileElement.click();
        return true;
    }

    public static boolean enterDataInFieldWeb(WebElement mobileElement, String dataToEnter,String fieldName, String screenName,int timeout)
    {
        stepLog= Report.getStepLog();
        WebDriverWait wait = new WebDriverWait(driver,timeout);
        try
        {
            wait.until(ExpectedConditions.visibilityOf(mobileElement));
        }
        catch (TimeoutException timeOut)
        {
            stepLog = CommonErrorExeptions.logElementCouldNotVisible(stepLog,fieldName,screenName);
            Log.error(timeOut);
            return false;
        }
        try
        {
            wait.until(ExpectedConditions.elementToBeClickable(mobileElement));
        }
        catch (TimeoutException timeOut)
        {
            stepLog = CommonErrorExeptions.logElementNotClickable(stepLog,fieldName,screenName);
            Log.error(timeOut);
            return false;
        }
        Report.setStepLog(stepLog);
        mobileElement.sendKeys(dataToEnter);
        return true;
    }



    public static boolean clickOnWebElement(WebElement webElement,String fieldName, String screenName, int timeout)
    {

        stepLog = Report.getStepLog();
        WebDriverWait wait = new WebDriverWait(driver,timeout);
        try
        {
            wait.until(ExpectedConditions.elementToBeClickable(webElement));
        }
        catch (TimeoutException timeOut)
        {
            stepLog = CommonErrorExeptions.logElementNotClickable(stepLog,fieldName,screenName);
            Log.error(timeOut.getMessage());
            return false;
        }
        webElement.click();
        Report.setStepLog(stepLog);
        return true;
    }


    public static void verifyElmentVisibleSoft(WebElement webElement)
    {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(getElementStatus(webElement,ElementStatus.VISIBLE),ElementStatus.VISIBLE);
    }

    public static ASSERT_STATUS verifyElmentVisibleHard(WebElement webElement)
    {
        Log.info("Function verifyElmentVisibleHard called ");
        try
        {
            Assert.assertEquals(getElementStatus(webElement,ElementStatus.VISIBLE),ElementStatus.VISIBLE);
        }
        catch (AssertionError assertionError)
        {
            Log.error("Element is not visible",assertionError);
            return ASSERT_STATUS.FAIL;
        }
        Log.info("Element is vissible and Assertion passed");
        Log.info("Function verifyElmentVisibleHard call finished ");
        return ASSERT_STATUS.PASS;
    }

    public static void verifyElmentEnabledSoft(WebElement webElement)
    {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(getElementStatus(webElement,ElementStatus.ENABLED),ElementStatus.ENABLED);
    }

    public static void verifyElmentEnabledHard(WebElement webElement)
    {
        Assert.assertEquals(getElementStatus(webElement,ElementStatus.ENABLED),ElementStatus.ENABLED);
    }

    public static void verifyWebElmentExistSoft(WebElement webElement)
    {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(getElementStatus(webElement,ElementStatus.PRESENT),ElementStatus.PRESENT);
    }

    public static void verifyWebElmentExistHard(WebElement webElement)
    {
        Assert.assertEquals(getElementStatus(webElement,ElementStatus.PRESENT),ElementStatus.PRESENT);
    }

    public static void verifyMobElmentExistSoft(MobileElement mobileElement)
    {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(getElementStatus(mobileElement,ElementStatus.PRESENT),ElementStatus.PRESENT);
    }

    public static void verifyMobElmentExistHard(MobileElement mobileElement)
    {
        Assert.assertEquals(getElementStatus(mobileElement,ElementStatus.PRESENT),ElementStatus.PRESENT);
    }


    public static void verifyWebElementTexttSoft(WebElement webElement, String actualText)
    {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(webElement.getText(),actualText);
    }

    public static void verifyWebElementTexttHard(WebElement webElement, String actualText)
    {
        Assert.assertEquals(webElement.getText(),actualText);
    }

    public static void verifyMobElementTexttSoft(MobileElement mobileElement, String actualText)
    {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(mobileElement.getText(),actualText);
    }

    public static void verifyMobElementTexttHard(MobileElement mobileElement, String actualText)
    {
        Assert.assertEquals(mobileElement.getText(),actualText);
    }


    public static MobileElement locateMobileElementBy(MobileBy by)
    {
        AndroidDriver driver = AndroidDriverInit.driver;
        WebDriverWait wait = new WebDriverWait(driver,15);
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
        if(isElementVisible(driver,by,ElementStatus.PRESENT)==ElementStatus.PRESENT)
        {
            return (MobileElement)driver.findElement(by);
        }
        else
        {
            return null;
        }

    }

    public static MobileElement locateMobileElementByResource(String locatorStrategy, String ResourceId, int index, String description)
    {
        MobileBy by = null;
        if(locatorStrategy.equalsIgnoreCase("Index"))
        {
             by =(MobileBy) MobileBy.AndroidUIAutomator(
                    "new UiSelector().resourceId("+ResourceId+")instance("+index+")");
        }
        else if(locatorStrategy.equalsIgnoreCase("text"))
        {
            String resourceText = driver.findElement(MobileBy.AndroidUIAutomator(
                    String.format("new UiSelector().resourceId(\"%s\")",ResourceId))).getText();

            by = (MobileBy) MobileBy.AndroidUIAutomator(
                    String.format("new UiSelector().text(\"%s\")", resourceText));
        }
        else if(locatorStrategy.equalsIgnoreCase("indexClickable"))
        {

            by = (MobileBy)MobileBy.AndroidUIAutomator(
                    String.format("new UiSelector().index("+index+").clickable(true).resourceId("+ResourceId+")"));
        }
        else if(locatorStrategy.equalsIgnoreCase("description"))
        {

            by = (MobileBy)MobileBy.AndroidUIAutomator(
                    String.format("new UiSelector().description(\"%s\")", description));
        }
        WebDriverWait wait = new WebDriverWait(driver,15);
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
        if(isElementVisible(driver,by,ElementStatus.PRESENT)==ElementStatus.PRESENT)
        {
            return  (MobileElement) driver.findElement(by);
        }
        else
        {
            return null;
        }

    }



    public static WebElement locateWebElementBy(By by)
    {
        WebElement element =null;
        try
        {
            WebDriverWait wait = new WebDriverWait(driver,15);
            wait.until(ExpectedConditions.presenceOfElementLocated(by));
            if(isElementVisible(driver,by,ElementStatus.PRESENT)==ElementStatus.PRESENT)
            {
                element= driver.findElement(by);
            }
        }
        catch(NullPointerException npe)
        {
            stepLog.append("Driver is null hence could not do a find on it");
             Log.info("Driver is null hence could not move ahead");
        }
        catch(TimeoutException te)
        {
            stepLog.append("Time Out Happened while trying to locate element");
            Log.info("Time Out Happened while trying to locate element");
        }
        if(element!=null)
        {
            return element;
        }
        else {
            return null;
        }
    }


    public enum ElementStatus{
        VISIBLE,
        NOTVISIBLE,
        ENABLED,
        NOTENABLED,
        PRESENT,
        NOTPRESENT
    }

    public enum ASSERT_STATUS
    {
        PASS,
        FAIL,
    }

    public static ElementStatus isElementVisible(AndroidDriver driver, By by, ElementStatus getStatus){

        try{
            if(getStatus.equals(ElementStatus.ENABLED)){
                if(driver.findElement(by).isEnabled())

                    return ElementStatus.ENABLED;
                return ElementStatus.NOTENABLED;

            }
            if(getStatus.equals(ElementStatus.VISIBLE)){
                if(driver.findElement(by).isDisplayed())
                    return ElementStatus.VISIBLE;
                return ElementStatus.NOTVISIBLE;
            }
            return ElementStatus.PRESENT;
        }catch(org.openqa.selenium.NoSuchElementException nse){
            return ElementStatus.NOTPRESENT;
        }
    }


    public static ElementStatus getElementStatus( WebElement webElement, ElementStatus getStatus){

        try{
            if(getStatus.equals(ElementStatus.ENABLED)){
                if(webElement.isEnabled())

                    return ElementStatus.ENABLED;
                return ElementStatus.NOTENABLED;

            }
            if(getStatus.equals(ElementStatus.VISIBLE)){
                if(webElement.isDisplayed())
                    return ElementStatus.VISIBLE;
                return ElementStatus.NOTVISIBLE;
            }
            return ElementStatus.PRESENT;
        }catch(org.openqa.selenium.NoSuchElementException nse){
            return ElementStatus.NOTPRESENT;
        }
    }

    public static boolean existsElement(String xpath,WebDriver driver) {
        try {
            driver.findElement(By.xpath(xpath));
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    public static By getByFromLocator(String locatorType, String locatorValue)
    {
        By by=null;
        switch (locatorType)
        {
            case "xpath":
                by = By.xpath(locatorValue);
                break;
            case "className":
                by = By.className(locatorValue);
                break;
            case "cssSelector":
                by = By.cssSelector(locatorValue);
                 break;
            case "id":
                by = By.id(locatorValue);
                 break;
            case "name":
                by = By.name(locatorValue);
            break;
            case "linkText":
                by = By.linkText(locatorValue);
                break;
            case "partialLinkText":
                by = By.partialLinkText(locatorValue);
                break;
            case "tagName":
                by = By.tagName(locatorValue);
                break;
             default:
                 break;
        }
        return by;
    }

    public static WebElement getWebElementElseLogError(String screenName,String fieldName)
    {
        String locatorStrag="";
        String locatorValue ="";
        StringBuffer stepLog= new StringBuffer();
        Log.info("Trying to get locator and locator type from file for field :-"+fieldName+" on screen :-"+screenName);
        locatorStrag = PrepearObjectRepo.getTheLocatorType(screenName,fieldName);
        if(locatorStrag==null)
        {
            stepLog=CommonErrorExeptions.logLocatorTypeNotFound(stepLog,fieldName,screenName);
            Log.info("Locator Type is null or empty for field "+fieldName+" to be present on :-"+screenName);
        }
        locatorValue = PrepearObjectRepo.getTheLocatorValue(screenName,fieldName);
        if(locatorValue==null)
        {
            stepLog=CommonErrorExeptions.logLocatorValueNotFound(stepLog,fieldName,screenName);
            Log.info("Locator Value is null or empty for field "+fieldName+" to be present on :-"+screenName);
        }
        if(Report.getCurrentStepStatus()!=Report.STATUS.FAIL)
        {
            By by = AppiumCommLib.getByFromLocator(locatorStrag,locatorValue);
            try
            {
                org.testng.Assert.assertNotNull(by);
                Log.info("By value retrived successfully for "+fieldName+" to be present on -:"+screenName);
            }
            catch (AssertionError assertionError)
            {
                stepLog=CommonErrorExeptions.logByValueNotFound(stepLog,fieldName,screenName);
                Log.error("By value could not be retrived successfully for :-"+fieldName+" to be present on -:"+screenName,assertionError);
            }
            if(Report.getCurrentStepStatus()!=Report.STATUS.FAIL)
            {
                WebElement element = AppiumCommLib.locateWebElementBy(by);
                try
                {
                    org.testng.Assert.assertNotNull(element);
                    Log.info("Element was successfully located:- "+fieldName+" to be present on :-"+screenName);
                    return element;
                }
                catch (AssertionError assertionError)
                {
                    stepLog=CommonErrorExeptions.logElementCouldNotBeLocated(stepLog,fieldName,screenName);
                    Log.error("Element could not be successfully located and it is null:- "+fieldName+" to be presend on :-"+screenName,assertionError);
                }
            }
        }
        Log.info("Proces finished to find locator and locator type from file for field :-"+fieldName+" on screen :-"+screenName);
        Report.setStepLog(stepLog);
        return null;
    }

    public static boolean checkIfWebElementExist(String screenName,String fieldName)
    {
        String locatorStrag="";
        String locatorValue ="";
        StringBuffer stepLog= new StringBuffer();
        Log.info("Trying to get locator and locator type from file for field :-"+fieldName+" on screen :-"+screenName);
        locatorStrag = PrepearObjectRepo.getTheLocatorType(screenName,fieldName);
        if(locatorStrag==null)
        {
            stepLog=CommonErrorExeptions.logLocatorTypeNotFound(stepLog,fieldName,screenName);
            Log.info("Locator Type is null or empty for field -:"+fieldName+" to be present on :-"+screenName);
        }
        locatorValue = PrepearObjectRepo.getTheLocatorValue(screenName,fieldName);
        if(locatorValue==null)
        {
            stepLog=CommonErrorExeptions.logLocatorValueNotFound(stepLog,fieldName,screenName);
            Log.info("Locator Value is null or empty for field :"+fieldName+" to be present on :-"+screenName);
        }
        if(Report.getCurrentStepStatus()!=Report.STATUS.FAIL)
        {
            By by = AppiumCommLib.getByFromLocator(locatorStrag,locatorValue);
            try
            {
                org.testng.Assert.assertNotNull(by);
                Log.info("By value retrived successfully for :"+fieldName+" to be present on :-"+screenName);
            }
            catch (AssertionError assertionError)
            {
                stepLog=CommonErrorExeptions.logByValueNotFound(stepLog,fieldName,screenName);
                Log.error("By value could not be retrived successfully for :"+fieldName+" to be present on :-"+screenName,assertionError);
            }
            if(Report.getCurrentStepStatus()!=Report.STATUS.FAIL)
            {
                WebElement element = AppiumCommLib.locateWebElementBy(by);
                if(element==null)
                {
                    Log.info("Element was not found : "+fieldName+" to be present on"+screenName);
                    stepLog.append("Element was not found : "+fieldName+" to be present on :-"+screenName);
                    return false;
                }
                else
                {
                    Log.info("Element was found and located: "+fieldName+" to be present on"+screenName);
                    stepLog.append("Element was found and located: "+fieldName+" to be present on :-"+screenName);
                    return true;
                }
            }
        }
        Log.info("Proces finished to find locator and locator type from file for field :"+fieldName+" on screen :-"+screenName);
        Report.setStepLog(stepLog);
        return false;
    }


    public static void driverLetsHoldForSometime(long forTime)
    {
        driver.manage().timeouts().implicitlyWait(forTime,TimeUnit.SECONDS);
    }

}
