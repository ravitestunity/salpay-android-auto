package com.salarium.android.testautomation.commonlibrary;


import org.apache.log4j.Logger;

import java.io.IOException;

public class CommonErrorExeptions {
    private static Logger log;
    private static String errorMessage;
    static
    {
        try {
            log=Utility.getTheLogger("CommonErrorExceptions");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static StringBuffer logLocatorTypeNotFound(StringBuffer stepLog,String fieldName,String screenName)
    {
        errorMessage = "Locator type could not be feteched for field "+fieldName+ "for screen : "+screenName;
        stepLog.append(errorMessage);
        log.error(errorMessage);
        Report.setCurrentStepStatus(Report.STATUS.FAIL);
        return stepLog;
    }

    public static StringBuffer logLocatorValueNotFound(StringBuffer stepLog,String fieldName,String screenName)
    {
        errorMessage="Locator Value could not be feteched for field "+fieldName+"for screen : "+screenName;
        stepLog.append(errorMessage);
        log.error(errorMessage);
        Report.setCurrentStepStatus(Report.STATUS.FAIL);
        return stepLog;
    }

    public static StringBuffer logByValueNotFound(StringBuffer stepLog,String fieldName,String screenName)
    {
        errorMessage = "By value for are not correct, pass the correct locator and locator type for "+fieldName+" for screen -:"+screenName+"\n";
        stepLog.append( errorMessage);
        Report.setCurrentStepStatus(Report.STATUS.FAIL);
       log.error(errorMessage);
        return stepLog;
    }

    public static StringBuffer logElementCouldNotBeLocated(StringBuffer stepLog,String fieldName,String screenName)
    {
        errorMessage = "Element with name "+fieldName+" could not be located"+"On screen-:"+screenName+"\n";
        stepLog.append(errorMessage);
        log.error(errorMessage);
        Report.setCurrentStepStatus(Report.STATUS.FAIL);
        return stepLog;
    }

    public static StringBuffer logElementNotClickable(StringBuffer stepLog,String fieldName,String screenName)
    {
        errorMessage = "Element with name "+fieldName+" is not clickable"+"On screen-:"+screenName+"\n";
        stepLog.append(errorMessage);
        log.error(errorMessage);
        Report.setCurrentStepStatus(Report.STATUS.FAIL);
        return stepLog;
    }

    public static StringBuffer logElementCouldNotVisible(StringBuffer stepLog,String fieldName,String screenName)
    {

        errorMessage = "Element :- "+fieldName+" was not visible on screen - "+screenName;
        stepLog.append(errorMessage);
        log.error(errorMessage);
        Report.setCurrentStepStatus(Report.STATUS.FAIL);
        return stepLog;
    }
}
