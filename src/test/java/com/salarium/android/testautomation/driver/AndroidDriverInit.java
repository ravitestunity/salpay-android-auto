package com.salarium.android.testautomation.driver;

import com.salarium.android.testautomation.commonlibrary.Report;
import com.salarium.android.testautomation.commonlibrary.Utility;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.log4j.Logger;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class AndroidDriverInit {

 public static AndroidDriver<AndroidElement> driver= null ;
 static DesiredCapabilities dc = new DesiredCapabilities();
 static Logger log;
 static StringBuffer stepLog = Report.getStepLog();
 static String message;

 public static void setUp() throws  InterruptedException {
     Report.setCurrentStepStatus(Report.STATUS.IN_PROGRESS);
	 File classpathRoot = new File(System.getProperty("user.dir"));
	 File appDir = new File(classpathRoot, "/App");
	 File app = new File(appDir, "app.apk");
     try {
          log = Utility.getTheLogger("AndroidDriverInit");
     } catch (IOException e) {
         e.printStackTrace();
         stepLog.append("Logger File Coud not be created  or read");
     }
     log.info("Fucntion SetUp called");
     //DesiredCapabilities capabilities = new DesiredCapabilities();
	 dc.setCapability(CapabilityType.BROWSER_NAME, "");
	 dc.setCapability("VERSION", "7.1.1");
	 dc.setCapability("deviceName", "Google Nexus 5-4.4.4 Emulator");
	 dc.setCapability("platformName", "Android");
	 dc.setCapability("app", app.getAbsolutePath());
	 dc.setCapability(MobileCapabilityType.FULL_RESET, false);
	 dc.setCapability("no-reset",  false);
	 dc.setCapability("autoGrantPermissions", true);
	 dc.setCapability("appPackage", "com.salarium.android.salpay");
	 dc.setCapability("appActivity", "com.salarium.android.salpay.activities.SplashActivity");

     try {

         driver = new AndroidDriver<AndroidElement>(new URL("http://0.0.0.0:4723/wd/hub"), dc);

     } catch (MalformedURLException e) {
         stepLog.append("URL is not correct of the Appium Server");
         e.printStackTrace();
         log.error("URL is not correct of the Appium Server",e);
         Report.setCurrentStepStatus(Report.STATUS.FAIL);
     }
     catch(org.openqa.selenium.WebDriverException webDriverException)
     {
         Report.setCurrentStepStatus(Report.STATUS.FAIL);
         stepLog.append("Driver object could not be created");
         log.error("Driver object could not be created",webDriverException);
     }
     finally {
         if(Report.getCurrentStepStatus()!=Report.STATUS.FAIL)
         {
             message = "Driver object created successfully";
             Report.setCurrentStepStatus(Report.STATUS.PASS);
             stepLog.append(message);
             log.info(message);
             Utility.takeScreenshot(driver,Report.getCurrentStepName());
         }
         Report.setStepLog(stepLog);
     }
     log.info("Function SetUp call finished");
 }

 public static void tearDown()
 {
     log.info("TeadDown function called");
     if(AndroidDriverInit.driver!=null)
     {
         stepLog = Report.getStepLog();
         Report.setCurrentStepStatus(Report.STATUS.IN_PROGRESS);
         if(driver!=null)
         {
             message = "Driver Could be closed successfully";
             driver.quit();
             driver=null;
             Report.setCurrentStepStatus(Report.STATUS.PASS);
             stepLog.append(message);
             log.info(message);

         }
         else
         {
             message = "Driver is already closed, so can not close again";
             Report.setCurrentStepStatus(Report.STATUS.FAIL);
             stepLog.append(message);
             log.error(message);
         }
         Report.setStepLog(stepLog);
     }
     log.info("TeadDown function call finished");
 }

 public static void verifyDriverClosed()
 {
     log.info("verifyDriverClosed function called");
     if(Report.checkPreviousStepStatusNotFailNoRun())
     {
         stepLog = Report.getStepLog();
         Report.setCurrentStepStatus(Report.STATUS.IN_PROGRESS);
         if(driver==null)
         {
             message = "Driver Could be closed successfully";
             Report.setCurrentStepStatus(Report.STATUS.PASS);
             stepLog.append(message);
             log.info(message);
         }
         else
         {
             message = "Driver is already closed, hence can not close it again";
             Report.setCurrentStepStatus(Report.STATUS.FAIL);
             stepLog.append(message);
             log.error(message);
         }
         Report.setStepLog(stepLog);
     }
     log.info("verifyDriverClosed function call finished");
 }
}