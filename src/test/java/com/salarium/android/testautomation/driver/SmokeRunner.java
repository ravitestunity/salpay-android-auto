
package com.salarium.android.testautomation.driver;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.junit.runner.RunWith;

@CucumberOptions(
		features = {"src/test/java/com/salarium/android/testautomation/scenarios/smoketest/SmokeTest.feature"},
		glue = {"com.salarium.android.testautomation.stepDefination"},
		plugin = {"pretty","html:target/cucumber-html-report",
				"junit:target/cucumber-reports/Cucumber.xml",
		"junit:target/cucumber-junit-report/allcukes.xml",
		"json:target/cucumber-report.json",
		"json:target/cucumber.json",
		"usage:target/cucumber-usage.json"},
		//tags = {" @SaveContractMBCA "
		monochrome= true
		)
//extends AbstractTestNGCucumberTests
//@RunWith(Cucumber.class)
public class SmokeRunner extends AbstractTestNGCucumberTests {

}
	


