@tag
Feature: Mobile
  Testing the Send Money functionality

  @tag1
  Scenario Outline: 
    Given Open App
    Given Login using "<UserName>" and "<Password>" click ok
    #=====SALPAY_SM_001=====#
    Then Verify Send Money button on quick options
    #=====SALPAY_SM_002=====#
    Then Verify Send Money Options
    #=====SALPAY_SM_003=====#
    Then Verify Back icon on the send money options page
    #=====SALPAY_SM_004=====#
    Then Verify send money to bank form
    #=====SALPAY_SM_005=====#
    Then Verify Back icon on the send money form page
    #=====SALPAY_SM_005=====#
    #Then Verify Cancel icon on the send money page(--no cancel icon exists)
    #=====SALPAY_SM_006=====#
    Then Verify List of Banks
    #=====SALPAY_SM_007=====#
    Then Verify Additional Information icon on Account Name
    #=====SALPAY_SM_008=====#
    Then Verify Additional Information icon on Account Number
    #=====SALPAY_SM_009=====#
    Then Verify Amount Number format
    #=====SALPAY_SM_010=====#
    #Then Verify Clear icon when Amount text field is blank(--clear icon element not defined by the development team)
    #=====SALPAY_SM_011=====#
    #Then Verify Clear icon when Amount is filled Account used:    (--clear icon element not defined by the development team)
    #=====SALPAY_SM_012=====#
    Then Tap CALCULATE FEES button when send money form is blank
    #=====SALPAY_SM_013=====#
    #Then Tap CALCULATE FEES button when Bank Name is not selected(--By default"Banco de oro" bank is selected)
    #=====SALPAY_SM_014=====#
    Then Tap CALCULATE FEES button when Account Name is blank
    #=====SALPAY_SM_015=====#
    Then Tap Verify Allowed Cahracter input on Account Name field
    #=====SALPAY_SM_016=====#
    Then Tap CALCULATE FEES button when Account Number is blank
    #=====SALPAY_SM_017=====#
    Then Tap CALCULATE FEES button when Account Number is less than or greater than the required digits
    #=====SALPAY_SM_018=====#
    Then Tap CALCULATE FEES button when Amount is blank
    #=====SALPAY_SM_019=====#
    Then Tap CALCULATE FEES button when Amount Entered is less than or greater than the required value
    #=====SALPAY_SM_020=====#
    Then Tap Send Money when SALPAy balance is insufficient to send the Amount
    #=====SALPAY_SM_021=====#
    Then Change bank name when Send money Form is filled
    #=====SALPAY_SM_022=====#
    Then Verify processing details
    #=====SALPAY_SM_023=====#
    Then Update Account name when processing fee is already calculated
    #=====SALPAY_SM_024=====#
    Then Update Account Number when processing fee is already calculated
    #=====SALPAY_SM_025=====#
    Then Update Amount when processing fee is already calculated
    #=====SALPAY_SM_026=====#
    Then Send money verification page
    #=====SALPAY_SM_027=====#
    Then Verify Back icon on the send money verification page
    #=====SALPAY_SM_028=====#
    Then Resubmit the Send Money form
    #=====SALPAY_SM_029=====#
    #Then Verify Cancel icon on the send money verification page #(--  No cancel button exists)
    #=====SALPAY_SM_031=====#
    Then Input incorrect verification code
    #=====SALPAY_SM_032=====#
    #Then Verify Resend link text  #(--can't verify as the resend otp acknowledgement message is not locatable)
    #=====SALPAY_SM_033=====#
    #Then Input the old verification code   #(--can't verify as we dont have old otp)
    #=====SALPAY_SM_034=====#
    Then Verify Transaction when account Balance is insufficient
    #=====SALPAY_SM_035=====#
    Then Input correct verification code
    #=====SALPAY_SM_036=====#
    Then Return to home screen
    #=====SALPAY_SM_037=====#
    Then Verify Transaction History

    # Then Click on Send Money
    # Then calculate fees
    Examples: 
      | UserName                       | Password |
      | korina.sanga+ssp1@salarium.com |     1234 |
    #Then Open App2
