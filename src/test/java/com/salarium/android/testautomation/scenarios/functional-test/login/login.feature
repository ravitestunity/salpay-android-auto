Feature: To verify all the Smoke Test Cases for SalPay App.

  Scenario Outline: Verify the Login Process for valid use case.
    Given I have opened the SalPay App.
    And I am on Login Screen
    When I enter "<User_Name>" & "<Password>"
    And I click on Login button
    Then I should be able to login
    And I should see the home page.
    When I click on Logout
    Then I should be logged out
    And I should again see the login screen.
    Examples:
      | User_Name                      | Password |
      | korina.sanga+ssp1@salarium.com | 1234     |


  Scenario Outline: Verify the Login Process for invalid use case.
    Given I have opened the SalPay App.
    And I am on Login Screen
    When I enter "<User_Name>" & "<Password>"
    And I click on Login button
    Then I should NOT be able to login
    And I should see the login screen itself.
    Examples:
      | User_Name                      | Password |
      | korina.sanga+ssp1@salarium.com | 4321     |