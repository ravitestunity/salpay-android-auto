Feature: To verify all the Smoke Test Cases for SalPay App.

  Scenario: Verify the Login Process for valid use case.
    Given I have opened the SalPay App.
    And I am on Login Screen
    When I enter user id and password and hit login
    And if passcode is asked I enter that
    Then I should be able to login
    And I should see the home page.
    And I should see My Accounts details
    And I should see browse money option
    And I should see quick options
    And I should see amount
    When I close the app
    Then It should get closed
