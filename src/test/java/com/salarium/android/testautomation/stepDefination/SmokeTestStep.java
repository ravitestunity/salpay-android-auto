package com.salarium.android.testautomation.stepDefination;

import com.salarium.android.testautomation.commonlibrary.PrepearTestData;
import com.salarium.android.testautomation.commonlibrary.Report;
import com.salarium.android.testautomation.commonlibrary.Utility;
import com.salarium.android.testautomation.driver.AndroidDriverInit;
import com.salarium.android.testautomation.pageobjects.HomeScreen;
import com.salarium.android.testautomation.pageobjects.LoginScreen;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.HashMap;
import java.util.Map;

public class SmokeTestStep {

    @Before
    public static void setTheSceanio(Scenario sceanio)
    {
        Utility.setFeatureNameScenarioName(sceanio);
    }

    @Given("^I have opened the SalPay App\\.$")
    public void i_have_opened_the_SalPay_App() throws Throwable {
        Report.addStepToScenaio("I have opened the SalPay App",Report.STATUS.NOT_RUN);
        // Write code here that turns the phrase above into concrete actions
        AndroidDriverInit.setUp();
        Report.setStepScenarioStatusWriteToHTMLAtStepEnd(false);
    }

    @Given("^I am on Login Screen$")
    public void i_am_on_Login_Screen() throws Throwable {
        Report.addStepToScenaio("I am on Login Screen",Report.STATUS.NOT_RUN);
        // Write code here that turns the phrase above into concrete actions
        LoginScreen lg = new LoginScreen();
        lg.verifyUserOnLoginScreen();
        Report.setStepScenarioStatusWriteToHTMLAtStepEnd(false);
    }


    @When("^I enter user id and password and hit login$")
    public void iEnterUserIdAndPasswordAndHitLogin() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Report.addStepToScenaio("I enter user id and password and hit login",Report.STATUS.NOT_RUN);
        Map<String,String> smokeTestData = new HashMap<String, String>();
        smokeTestData = PrepearTestData.getTheTestData();
        String UserId = smokeTestData.get("UserId");
        String Password = smokeTestData.get("Password");
        LoginScreen loginScreen = new LoginScreen();
        loginScreen.doLogin(UserId,Password);
        Report.setStepScenarioStatusWriteToHTMLAtStepEnd(false);
    }

    @Then("^I should be able to login$")
    public void i_should_be_able_to_login() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Report.addStepToScenaio("I should be able to login",Report.STATUS.NOT_RUN);
        LoginScreen loginScreen = new LoginScreen();
        loginScreen.verifyUserLoggedInSucessfully();
        Report.setStepScenarioStatusWriteToHTMLAtStepEnd(false);
    }

    @Then("^I should see the home page\\.$")
    public void i_should_see_the_home_page() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Report.addStepToScenaio("I should see the home page",Report.STATUS.NOT_RUN);
        HomeScreen homeScreen = new HomeScreen();
        homeScreen.verifyUserOnHomePage();
        Report.setStepScenarioStatusWriteToHTMLAtStepEnd(false);
    }

    @Then("^I should see My Accounts details$")
    public void i_should_see_My_Accounts_details() throws Throwable {
        Report.addStepToScenaio("I should see My Accounts details",Report.STATUS.NOT_RUN);
        HomeScreen homeScreen = new HomeScreen();
        homeScreen .verifyAccountDetailsPresent();
        Report.setStepScenarioStatusWriteToHTMLAtStepEnd(false);
    }

    @Then("^I should see browse money option$")
    public void i_should_see_browse_money_option() throws Throwable {
        Report.addStepToScenaio("I should see browse money option",Report.STATUS.NOT_RUN);
        HomeScreen homeScreen = new HomeScreen();
        homeScreen.verifyFlashLoanOptionPresent();
        homeScreen.verifyEasyLoanOptionPresent();
        Report.setStepScenarioStatusWriteToHTMLAtStepEnd(false);
    }

    @Then("^I should see quick options$")
    public void i_should_see_quick_options() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Report.addStepToScenaio("I should see quick options",Report.STATUS.NOT_RUN);
        HomeScreen homeScreen = new HomeScreen();
        homeScreen.verifyBrowMoneyPresent();
        homeScreen.verifyBuyLoadOptionPresent();
        homeScreen.verifyPayBillOptionPresent();
        Report.setStepScenarioStatusWriteToHTMLAtStepEnd(false);
    }

    @When("^I click on log out$")
    public void i_click_on_log_out() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("I am in Open Salpay");

    }

    @Then("^I should get logged out$")
    public void i_should_get_logged_out() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("I am in Open Salpay");
    }

    @Then("^I should again see login screen\\.$")
    public void i_should_again_see_login_screen() throws Throwable {
        Report.addStepToScenaio("I should again see login screen",Report.STATUS.NOT_RUN);
        // Write code here that turns the phrase above into concrete actions
        LoginScreen lg = new LoginScreen();
        lg.verifyUserOnLoginScreen();
        Report.setStepScenarioStatusWriteToHTMLAtStepEnd(false);
    }

    @When("^I close the app$")
    public void iCloseTheApp() throws Throwable {
        Report.addStepToScenaio("I close the app",Report.STATUS.NOT_RUN);
        AndroidDriverInit.tearDown();
        Report.setStepScenarioStatusWriteToHTMLAtStepEnd(false);
    }

    @Then("^It should get closed$")
    public void itShouldGetClosed() throws Throwable {
        Report.addStepToScenaio("It should get closed",Report.STATUS.NOT_RUN);
        AndroidDriverInit.verifyDriverClosed();
        Report.setStepScenarioStatusWriteToHTMLAtStepEnd(true);
    }

    @And("^if passcode is asked I enter that$")
    public void ifPasscodeIsAskedIEnterThat() throws Throwable {
        Report.addStepToScenaio("if passcode is asked I enter that",Report.STATUS.NOT_RUN);
        LoginScreen lg = new LoginScreen();
        lg.setThePasscode();
        Report.setStepScenarioStatusWriteToHTMLAtStepEnd(false);
    }

    @And("^I should see amount$")
    public void iShouldSeeAmount() throws Throwable {
        Report.addStepToScenaio("I should see amount",Report.STATUS.NOT_RUN);
    }
}
