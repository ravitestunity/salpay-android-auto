$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/com/salarium/android/testautomation/scenarios/smoketest/SmokeTest.feature");
formatter.feature({
  "line": 1,
  "name": "To verify all the Smoke Test Cases for SalPay App.",
  "description": "",
  "id": "to-verify-all-the-smoke-test-cases-for-salpay-app.",
  "keyword": "Feature"
});
formatter.before({
  "duration": 181979700,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Verify the Login Process for valid use case.",
  "description": "",
  "id": "to-verify-all-the-smoke-test-cases-for-salpay-app.;verify-the-login-process-for-valid-use-case.",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "I have opened the SalPay App.",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I am on Login Screen",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "I enter user id and password and hit login",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "if passcode is asked I enter that",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I should be able to login",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "I should see the home page.",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I should see My Accounts details",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I should see browse money option",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I should see quick options",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I should see amount",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I close the app",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "It should get closed",
  "keyword": "Then "
});
formatter.match({
  "location": "SmokeTestStep.i_have_opened_the_SalPay_App()"
});
formatter.result({
  "duration": 43320718308,
  "status": "passed"
});
formatter.match({
  "location": "SmokeTestStep.i_am_on_Login_Screen()"
});
formatter.result({
  "duration": 13545135873,
  "status": "passed"
});
formatter.match({
  "location": "SmokeTestStep.iEnterUserIdAndPasswordAndHitLogin()"
});
formatter.result({
  "duration": 71664526010,
  "status": "passed"
});
formatter.match({
  "location": "SmokeTestStep.ifPasscodeIsAskedIEnterThat()"
});
formatter.result({
  "duration": 22508067143,
  "status": "passed"
});
formatter.match({
  "location": "SmokeTestStep.i_should_be_able_to_login()"
});
formatter.result({
  "duration": 12973163924,
  "status": "passed"
});
formatter.match({
  "location": "SmokeTestStep.i_should_see_the_home_page()"
});
formatter.result({
  "duration": 704366671,
  "status": "passed"
});
formatter.match({
  "location": "SmokeTestStep.i_should_see_My_Accounts_details()"
});
formatter.result({
  "duration": 745278726,
  "status": "passed"
});
formatter.match({
  "location": "SmokeTestStep.i_should_see_browse_money_option()"
});
formatter.result({
  "duration": 1583167195,
  "status": "passed"
});
formatter.match({
  "location": "SmokeTestStep.i_should_see_quick_options()"
});
formatter.result({
  "duration": 2193133344,
  "status": "passed"
});
formatter.match({
  "location": "SmokeTestStep.iShouldSeeAmount()"
});
formatter.result({
  "duration": 87043,
  "status": "passed"
});
formatter.match({
  "location": "SmokeTestStep.iCloseTheApp()"
});
formatter.result({
  "duration": 4573840810,
  "status": "passed"
});
formatter.match({
  "location": "SmokeTestStep.itShouldGetClosed()"
});
formatter.result({
  "duration": 39954584,
  "status": "passed"
});
});