package com.sg.mobiletest.Mobiletesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.server.handler.SendKeys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.PressesKeyCode;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class Homepage {
	WebDriver driver = Salarium.driver;
	
	public void sendmoney() throws InterruptedException{
		TouchAction touchAction = new TouchAction(Salarium.driver);
		touchAction.press(93,1356).release().perform();
		Thread.sleep(2000);
		((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_BACK);
		Thread.sleep(2000);

	}
    public void buyload() throws InterruptedException{
    	//TouchAction touchAction = new TouchAction(Salarium.driver);
		//touchAction.press(388,1326).release().perform();
    	new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/rl_buy_load_container']")));
		driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/rl_buy_load_container']")).click();
		Thread.sleep(2000);

		((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_BACK);
		Thread.sleep(2000);

    }
    public void paybills() throws InterruptedException{
/*    	TouchAction touchAction = new TouchAction(Salarium.driver);
		touchAction.press(713,1326).release().perform();*/
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/rl_pay_bills_container']")));
		driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/rl_pay_bills_container']")).click();
		Thread.sleep(10000);
        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.KEYCODE_BACK);
		Thread.sleep(2000);

    	
    }
}
