package com.sg.mobiletest.Mobiletesting;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class SendMoney {
	
	WebDriver driver= Salarium.driver;
	TouchAction touchAction = new TouchAction(Salarium.driver);
	
	 Logger Log=Logger.getLogger(SendMoney.class);
	 
	 
     
	 
     // configure log4j properties file
     
	
	public void sendmoney_presence(){
		try {
			Thread.sleep(4000);
			touchAction.press(93,1356).release().perform();
			Thread.sleep(4000);
		
			touchAction.press(300,726).release().perform();
			new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/billerTextView']")));
			MobileElement iselementpresent = driver.findElement((By.xpath("//*[@resource-id='com.salarium.android.salpay:id/billerTextView']")));
			
			
			if(iselementpresent.isDisplayed()){
				System.out.println("Send money option visible from quick option");
				Log.info("Send money option visible from quick option");
			}
			else{
				System.out.println("Send money option not visible from quick option");
				Log.info("Send money option not visible from quick option");
		}
			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
			

			}
	    catch (Exception e) {
	    	
	    	((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
	    }
	}
		


	private void assertTrue(boolean displayed) {
		// TODO Auto-generated method stub
		
	}



	public  void sendmoney() throws InterruptedException{
		try {
			Thread.sleep(4000);
			touchAction.press(93,1356).release().perform();
			Thread.sleep(4000);
			touchAction.press(300,726).release().perform();
			new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/billerTextView']")));
			if(driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/billerTextView']")).isDisplayed()){
				System.out.println("Send money option working fine");
				Log.info("Send money option working fine");
			}
			else{
				System.out.println("Send money option not working fine");
				Log.info("Send money option not working fine");
		}
			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);

			}
	    catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	    }

		}
	public void sendmoneytoback(){
		try {
			Thread.sleep(4000);
			touchAction.press(93,1356).release().perform();
			Thread.sleep(4000);
			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
			new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/rl_buy_load_container']")));
			if(driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/rl_buy_load_container']")).isDisplayed()){
				System.out.println("Back option from Send money working fine");
				Log.info("Back option from Send money working fine");
			}
			else{
				System.out.println("Back option from Send money not working fine");
				Log.info("Back option from Send money not working fine");
		}
			//((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);

			}
	    catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	    }
	}
		
	
	public void bankform() throws InterruptedException{
		try {
			Thread.sleep(4000);
			touchAction.press(93,1356).release().perform();
			Thread.sleep(4000);
			touchAction.press(300,726).release().perform();
			Thread.sleep(3000);
			if(driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/billerTextView']")).isDisplayed()){
				System.out.println("Bank form opening properly");
				Log.info("Bank form opening properly");
			}
				else{
					System.out.println("Bank form not opening");
					Log.info("Bank form not opening");
				}
			 Thread.sleep(3000);
			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);

			
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		    }

		
	}
	public void bankformback(){
		try {
			Thread.sleep(4000);
			touchAction.press(93,1356).release().perform();
			Thread.sleep(4000);
			touchAction.press(300,726).release().perform();
			Thread.sleep(3000);
			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
			
			new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/rl_buy_load_container']")));
			if(driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/rl_buy_load_container']")).isDisplayed()){
				System.out.println("Back option from Bank form working fine");
				Log.info("Back option from Bank form working fine");
			}
			else{
				System.out.println("Back option from bank form not working fine");
				Log.info("Back option from bank form not working fine");
		}
			
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		    }
		
	}
	public void banklist(){
		try {
			Thread.sleep(4000);
		
		touchAction.press(93,1356).release().perform();
		Thread.sleep(4000);
		touchAction.press(300,726).release().perform();
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/billerTextView']")));
		driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/billerTextView']")).click();
		Thread.sleep(4000);
		if(driver.findElement(By.xpath("//*[@class='android.widget.FrameLayout']")).isDisplayed()){
			System.out.println("Bank list opening properly");
			Log.info("Bank list opening properly");
		}
		else{
			System.out.println("Bank list not opening properly");
			Log.info("Bank list not opening properly");
	}
		Thread.sleep(2000);
		((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	public void verifyadditionaliconaccountname(){
		try {
			Thread.sleep(4000);
		
		touchAction.press(93,1356).release().perform();
		Thread.sleep(4000);
		touchAction.press(300,726).release().perform();
		Thread.sleep(3000);
		touchAction.press(912,909).release().perform();
		/*new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/descriptionImageView'][2]")));
		driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/descriptionImageView'][2]")).click();
		Thread.sleep(4000);*/
		Thread.sleep(4000);
		if(driver.findElement(By.xpath("//*[@class='android.widget.ScrollView']//*[@resource-id='com.salarium.android.salpay:id/descriptionImageView']")).isDisplayed()){
			System.out.println("Account name verification icon displaying properly");
			Log.info("Account name verification icon displaying properly");
		}
		else{
			System.out.println("Account name verification icon displaying properly");
			Log.info("Account name verification icon displaying properly");
	}
		((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void verifyadditionaliconaccountnumber(){
		try {
			Thread.sleep(4000);
		
		touchAction.press(93,1356).release().perform();
		Thread.sleep(4000);
		touchAction.press(300,726).release().perform();
		Thread.sleep(4000);
		touchAction.press(912,1102).release().perform();
		Thread.sleep(2000);
		if(driver.findElement(By.xpath("//*[@class='android.widget.LinearLayout' and ./*[@class='android.widget.LinearLayout' and ./*[@text='Account Number']]]//*[@resource-id='com.salarium.android.salpay:id/descriptionLayout']")).isEnabled()){
			
			System.out.println("Account number verification icon displaying properly");
			Log.info("Account number verification icon displaying properly");
		}
		else{
			System.out.println("Account number verification icon displaying properly");
			Log.info("Account number verification icon displaying properly");
	}
		((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void verify_amount_number_format() throws InterruptedException{
		Thread.sleep(4000);
		
		touchAction.press(93,1356).release().perform();
		Thread.sleep(4000);
		touchAction.press(300,726).release().perform();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@text='Amount']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys("10000.00");
		Thread.sleep(4000);
		((AndroidDriver<?>) driver).hideKeyboard();
		String format= driver.findElement(By.xpath("//*[@text='Amount']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).getText();
		System.out.println(format);
		String text= "10,000.00";
		Thread.sleep(2000);
		if(format.equals(text)){
			
			System.out.println("Account number format is proper");
			Log.info("Account number format is proper");
		}
		else{
			System.out.println("Account number is not proper");
			Log.info("Account number is not proper");
	}
		Thread.sleep(4000);

		((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);

		
		
		
	}
		public void select_bank() throws InterruptedException{
			
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/billerTextView']")));
		driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/billerTextView']")).click();
		Thread.sleep(4000);
		
		driver.findElement(By.xpath("//*[@text='Banco de Oro (BDO)']")).click();
		}
		public void enter_accountname() throws InterruptedException{
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")));
		driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys("Jhone Christian Doe");
		((AndroidDriver<?>) driver).hideKeyboard();
		Thread.sleep(3000);
		}
		public void enter_account_number() throws InterruptedException{
		driver.findElement(By.xpath("//*[@text='Account Number']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys("00123456789");
		Thread.sleep(3000);
		try {
			((AndroidDriver<?>) driver).hideKeyboard();
            } catch (Exception e) {
            }
		}
		public void enter_amount() throws InterruptedException{
		
		driver.findElement(By.xpath("//*[@text='Amount']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys("1000");
		try {
			((AndroidDriver<?>) driver).hideKeyboard();
            } catch (Exception e) {
            }
		Thread.sleep(4000);
		}
		public void submit_calculate_fees() throws InterruptedException{
			
				Thread.sleep(4000);
			touchAction.press(93,1356).release().perform();
			Thread.sleep(4000);
			touchAction.press(300,726).release().perform();
			Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
		Thread.sleep(4000);
		((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
	
	}
		public void when_Account_Name_is_blank() throws Throwable {
			Thread.sleep(4000);
			touchAction.press(93,1356).release().perform();
			Thread.sleep(4000);
			touchAction.press(300,726).release().perform();
			Thread.sleep(4000);
			SendMoney sendmoneyoption = new SendMoney();
			sendmoneyoption.enter_account_number();
			sendmoneyoption.enter_amount();
			driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);

		   }
		public void when_Account_Number_is_blank() throws Throwable {
			Thread.sleep(4000);
			touchAction.press(93,1356).release().perform();
			Thread.sleep(4000);
			touchAction.press(300,726).release().perform();
			Thread.sleep(4000);
			SendMoney sendmoneyoption = new SendMoney();
			sendmoneyoption.enter_accountname();
			sendmoneyoption.enter_amount();
			driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);

		   }
		public void when_Amount_is_blank() throws Throwable {
			Thread.sleep(4000);
			touchAction.press(93,1356).release().perform();
			Thread.sleep(4000);
			touchAction.press(300,726).release().perform();
			Thread.sleep(4000);
			SendMoney sendmoneyoption = new SendMoney();
			
			sendmoneyoption.enter_account_number();
			Thread.sleep(4000);
			sendmoneyoption.enter_amount();
			driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
			Thread.sleep(2000);
			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);

		   }
		public void accountname_validation(){
			
			try {
				Thread.sleep(4000);
			
			touchAction.press(93,1356).release().perform();
			Thread.sleep(4000);
			touchAction.press(300,726).release().perform();
			Thread.sleep(4000);
			SendMoney sendmoneyoption = new SendMoney();
			String name= "abcdefghijklmnopqrstuvwxyzabcdefghijklmno";
	
			driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys(name);
			Thread.sleep(2000);
			((AndroidDriver<?>) driver).hideKeyboard();
			Thread.sleep(2000);
			sendmoneyoption.enter_account_number();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
			Thread.sleep(4000);
			MobileElement account_name_validation= 	driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/textinput_error']"));
           if (account_name_validation.isEnabled()){
        	   System.out.println("Account name validation is working properly");
           Log.info("Account name validation is working properly");
           }
           else{
        	   System.out.println("Account name validation is not working properly");
           Log.info("Account name validation is not working properly");
           }
			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

		   }
		
		public void account_number_validation_1() throws InterruptedException{
			
		    Thread.sleep(4000);
			
			touchAction.press(93,1356).release().perform();
			Thread.sleep(4000);
			touchAction.press(300,726).release().perform();
			Thread.sleep(4000);
			try{
			SendMoney sendmoneyoption = new SendMoney();
			sendmoneyoption.enter_accountname();
			String digit= "1";
			driver.findElement(By.xpath("//*[@text='Account Number']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys(digit);
			try{
				((AndroidDriver<?>) driver).hideKeyboard();
			}catch(Exception e){
				
			}

			Thread.sleep(4000);
			driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
			Thread.sleep(2000);

			MobileElement Min= driver.findElement(By.xpath("//*[@class='android.widget.LinearLayout']//*[@text='Account Number']//*[@class='android.widget.LinearLayout']//*[@resource-id='com.salarium.android.salpay:id/textinput_error']"));

	       if(Min.isDisplayed()){
	    	   System.out.println("Validation for below minimum digit is working as expected");
	    	   Log.info("Validation for below minimum digit is working as expected");
	       }
	    	   
	       
	       else{
	    	   System.out.println("Validation for below minimum digit is not working as expected");
	    	   Log.info("Validation for below minimum digit is not working as expected");
	    	   
	       }}catch(Exception e){
	    	   e.printStackTrace();
	       }
		}
		public void account_number_validation_2(){
	    	
	    		   try{
	    			   Thread.sleep(3000);
	    				SendMoney sendmoneyoption = new SendMoney();
	    				String digit2="123456789123456789";
	    				driver.findElement(By.xpath("//*[@text='Account Number']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).clear();
	    				driver.findElement(By.xpath("//*[@text='Account Number']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys(digit2);
	    				try{
	    				((AndroidDriver<?>) driver).hideKeyboard();
	    				}catch(Exception e){
	    					
	    				}

	    				Thread.sleep(4000);
	    				driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
	    				Thread.sleep(4000);
                        MobileElement max= driver.findElement(By.xpath("//*[@class='android.widget.LinearLayout']//*[@text='Account Number']//*[@class='android.widget.LinearLayout']//*[@resource-id='com.salarium.android.salpay:id/textinput_error']"));
                        if(max.isDisplayed()){
	    		    	   System.out.println("Validation for above maximum digit is working as expected");
	    		    	   Log.info("Validation for above maximum digit is working as expected");
	    		       }
	    		       else{
	    		    	   System.out.println("Validation for above maximum digit is not working as expected");
	    		    	   Log.info("Validation for above maximum digit is not working as expected");
	    		       }
	    		       }catch(Exception e){
	    		    	   e.printStackTrace();
	    		    	   }
	   			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);

	    		   
	       }
		public void Amount_validation_1() throws InterruptedException{
            Thread.sleep(4000);
			touchAction.press(93,1356).release().perform();
			Thread.sleep(4000);
			touchAction.press(300,726).release().perform();
			Thread.sleep(4000);
			try{
			SendMoney sendmoneyoption = new SendMoney();
			sendmoneyoption.enter_accountname();
			sendmoneyoption.enter_account_number();

			String digit= "10";
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@text='Amount']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys(digit);
			try{
			((AndroidDriver<?>) driver).hideKeyboard();
			}catch(Exception e)
			{
				
			}
			driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
			Thread.sleep(4000);
			
	       if(driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/textinput_error']")).isDisplayed()){
	    	   System.out.println("Validation for below minimum amount is working as expected");
	    	   Log.info("Validation for below minimum amount is working as expected");
	       }
	    	   
	       
	       else{
	    	   System.out.println("Validation for below minimum amount is not working as expected");
	    	   Log.info("Validation for below minimum amount is not working as expected");
	    	   
	       }}catch(Exception e){
	       }
		}
	    	   public void Amount_validation_2(){
	    	
	    		   try{
	    				SendMoney sendmoneyoption = new SendMoney();
	    				String digit2="123456789123456789";
	    				Thread.sleep(2000);
	    				driver.findElement(By.xpath("//*[@text='Amount']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).clear();

	    				driver.findElement(By.xpath("//*[@text='Amount']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys(digit2);
	    				try{
	    				((AndroidDriver<?>) driver).hideKeyboard();
	    				}
	    				catch(Exception e){
	    					
	    				}
	    				driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
	    				Thread.sleep(4000);
	    				
	    		       if(driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/textinput_error']")).isDisplayed()){
	    		    	   System.out.println("Validation for above maximum amount is working as expected");
	    		    	   Log.info("Validation for above maximum amount is working as expected");
	    		       }
	    		    	   
	    		       
	    		       else{
	    		    	   System.out.println("Validation for above maximum amount is not working as expected");
	    		    	   Log.info("Validation for above maximum amount is not working as expected");
	    		    	   
	    		       }}catch(Exception e){
			
			
		}
	   			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
			
		}
	    	   
	    	   public void SALPAy_balance_insufficient() throws InterruptedException{
	    		   Thread.sleep(3000);
	    		   touchAction.press(718,330).release().perform();
	    		   Thread.sleep(3000);

	    		  String balance= driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/cv_balance_container']//*[@resource-id='com.salarium.android.salpay:id/tv_amount']")).getText();
	    		  //balance = balance.replaceAll("[\\D.]", "");
	    		  balance = balance.replaceAll("[\\D.]", "");
	    		  String balance1=balance.substring(0, balance.length() - 2);
	    		  //System.out.println(balance1);
	    		  
	    		 int Balance_int = Integer.parseInt(balance1);
	    		  int number_to_add=5;
	    		  int add=Balance_int+number_to_add;
	    		  String add1= String.valueOf(add);

		   			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
		   			Thread.sleep(4000);
					touchAction.press(93,1356).release().perform();
					Thread.sleep(4000);
					touchAction.press(300,726).release().perform();
					Thread.sleep(4000);
					SendMoney sendmoneyoption = new SendMoney();
					sendmoneyoption.enter_accountname();
					sendmoneyoption.enter_account_number();
					driver.findElement(By.xpath("//*[@text='Amount']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys(add1);
					((AndroidDriver<?>) driver).hideKeyboard();

					driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
					Thread.sleep(4000);

					driver.findElement(By.xpath("//*[@text='SEND MONEY']")).click();
					Thread.sleep(4000);


			
					if(driver.findElement(By.xpath("//*[@text='SEND MONEY']")).isDisplayed()){
						System.out.println("Verification successful if entering more than available amount");
						Log.info("Verification successful if entering more than available amount");
					}
					else{
						System.out.println("Verification unsuccessful if entering more than available amount");
						Log.info("Verification unsuccessful if entering more than available amount");
					}
					
					((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);

	    	   }
	    		  
	    	   
	    	   
	    	   
	    	   public void change_bank1() throws InterruptedException{
	    		   Thread.sleep(4000);
	    			touchAction.press(93,1356).release().perform();
	    			Thread.sleep(4000);
	    			touchAction.press(300,726).release().perform();
	    			Thread.sleep(4000);
	    			SendMoney sendmoneyoption = new SendMoney();
	    			sendmoneyoption.enter_accountname();
	    			sendmoneyoption.enter_account_number();
	    			sendmoneyoption.enter_amount();
	    			Thread.sleep(4000);
	    			new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/billerTextView']")));
	    			driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/billerTextView']")).click();
	    			Thread.sleep(4000);
	    			
	    			driver.findElement(By.xpath("//*[@text='Chinabank']")).click();
                    Thread.sleep(3000);
                    String amount =driver.findElement(By.xpath("//*[@text='Amount']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).getText();
                    String Accountname=driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).getText();
                    String Accountnumber=driver.findElement(By.xpath("//*[@text='Account Number']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).getText();

                    if((amount).isEmpty()||(Accountname).isEmpty()||(Accountnumber).isEmpty()){
                    	System.out.println("bank update working properly");
                    	Log.info("bank update working properly");
                    }		
        			else{
        				System.out.println("bank update not working properly");
        				Log.info("bank update not working properly");
        			}
					((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);

                    
	    	   }
		
		public void processing_detail() throws InterruptedException{
			Thread.sleep(4000);
			touchAction.press(93,1356).release().perform();
			Thread.sleep(4000);
			touchAction.press(300,726).release().perform();
			Thread.sleep(4000);
			SendMoney sendmoneyoption = new SendMoney();
			sendmoneyoption.enter_accountname();

			sendmoneyoption.enter_account_number();
			sendmoneyoption.enter_amount();
			driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
			Thread.sleep(4000);
			if(driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/transactionDetailsLayout']")).isDisplayed()){
				System.out.println("processing detail coming properly");
				Log.info("processing detail coming properly");
			}
			else{
				System.out.println("processing detail not working properly");
				Log.info("processing detail not working properly");
			}
			
			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
			
		}
		
		public void change_bank(){
			
		}
		public void update_Account_name() throws InterruptedException{
			Thread.sleep(4000);
			touchAction.press(93,1356).release().perform();
			Thread.sleep(4000);
			touchAction.press(300,726).release().perform();
			Thread.sleep(4000);
			SendMoney sendmoneyoption = new SendMoney();
			sendmoneyoption.enter_accountname();
			sendmoneyoption.enter_account_number();
			sendmoneyoption.enter_amount();
			driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
			Thread.sleep(4000);
			driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).clear();

			driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys("Bhupesh Gupta");
			((AndroidDriver<?>) driver).hideKeyboard();
			Thread.sleep(3000);

			if(driver.findElement(By.xpath("//*[@text='SEND MONEY']")).isDisplayed()){
				System.out.println("updating account name coming properly");
				Log.info("updating account name coming properly");
			}
			else{
				System.out.println("updating account name not coming properly");
				Log.info("updating account name not coming properly");
			}
			
			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
			
			
		}
			
		
public void update_Account_number() throws InterruptedException{
	Thread.sleep(4000);
	touchAction.press(93,1356).release().perform();
	Thread.sleep(4000);
	touchAction.press(300,726).release().perform();
	Thread.sleep(4000);
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.enter_accountname();

	sendmoneyoption.enter_account_number();
	sendmoneyoption.enter_amount();
	driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@text='Account Number']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).clear();

	driver.findElement(By.xpath("//*[@text='Account Number']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys("9876543214");
	((AndroidDriver<?>) driver).hideKeyboard();
	Thread.sleep(3000);

	if(driver.findElement(By.xpath("//*[@text='SEND MONEY']")).isDisplayed()){
		System.out.println("updating account number coming properly");
		Log.info("updating account number coming properly");
	}
	else{
		System.out.println("updating account number not working properly");
		Log.info("updating account number not working properly");
	}
	
	((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
	
}	
		
public void update_Amount() throws InterruptedException{
	Thread.sleep(4000);
	touchAction.press(93,1356).release().perform();
	Thread.sleep(4000);
	touchAction.press(300,726).release().perform();
	Thread.sleep(4000);
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.enter_accountname();

	sendmoneyoption.enter_account_number();
	sendmoneyoption.enter_amount();
	driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@text='Amount']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).clear();

	driver.findElement(By.xpath("//*[@text='Amount']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys("5000");
	((AndroidDriver<?>) driver).hideKeyboard();
	Thread.sleep(3000);

	if(driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).isDisplayed()){
		System.out.println("updating amount coming properly");
		Log.info("updating amount coming properly");
	}
	else{
		System.out.println("updating amount not working properly");
		Log.info("updating amount not working properly");
	}
	
	((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
	
}	

public void send_money_verification_page() throws InterruptedException{
	Thread.sleep(4000);
	touchAction.press(93,1356).release().perform();
	Thread.sleep(4000);
	touchAction.press(300,726).release().perform();
	Thread.sleep(4000);
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.enter_accountname();

	sendmoneyoption.enter_account_number();
	sendmoneyoption.enter_amount();
	driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@text='SEND MONEY']")).click();
	Thread.sleep(4000);
	MobileElement confirm_transaction= driver.findElement(By.xpath("//*[@text='Confirm Transaction']"));
	
    if(confirm_transaction.isDisplayed()){
		System.out.println("send money verification page successful");
		Log.info("send money verification page successful");
	}
	else{
		System.out.println("send money verification page unsuccessful");
		Log.info("send money verification page unsuccessful");
	}
	
	((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
	
}

public void Resubmit_send_money_verification_page() throws InterruptedException{
	Thread.sleep(4000);
	touchAction.press(93,1356).release().perform();
	Thread.sleep(4000);
	touchAction.press(300,726).release().perform();
	Thread.sleep(4000);
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.enter_accountname();

	sendmoneyoption.enter_account_number();
	sendmoneyoption.enter_amount();
	driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@text='SEND MONEY']")).click();
	Thread.sleep(3000);

	((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
	
}
public void back_from_send_money_verification_page() throws InterruptedException{
	Thread.sleep(4000);
	touchAction.press(93,1356).release().perform();
	Thread.sleep(4000);
	touchAction.press(300,726).release().perform();
	Thread.sleep(4000);
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.enter_accountname();

	sendmoneyoption.enter_account_number();
	sendmoneyoption.enter_amount();
	driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@text='SEND MONEY']")).click();
	Thread.sleep(2000);

		((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
	
}


public void Input_incorrect_verification_code () throws InterruptedException{
	Thread.sleep(4000);
	touchAction.press(93,1356).release().perform();
	Thread.sleep(4000);
	touchAction.press(300,726).release().perform();
	Thread.sleep(4000);
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.enter_accountname();

	sendmoneyoption.enter_account_number();
	sendmoneyoption.enter_amount();
	driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@text='SEND MONEY']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@class='android.widget.EditText']")).click();
	Thread.sleep(4000);
	((AndroidDriver<?>) driver).getKeyboard().sendKeys("1234");
	Thread.sleep(4000);
    MobileElement otp_error_msg=driver.findElement(By.xpath("//*[@text='Invalid verification code']"));
	if(otp_error_msg.isDisplayed()){
		System.out.println("Error message displayed after entering wrong OTP");
		Log.info("Error message displayed after entering wrong OTP");
	}
	else{
		System.out.println("Error message not displayed after entering wrong OTP");
		Log.info("Error message not displayed after entering wrong OTP");
	}
	
	((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
	
}

public void Resend_verification_code () throws InterruptedException{
	Thread.sleep(4000);
	touchAction.press(93,1356).release().perform();
	Thread.sleep(4000);
	touchAction.press(300,726).release().perform();
	Thread.sleep(4000);
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.enter_accountname();

	sendmoneyoption.enter_account_number();
	sendmoneyoption.enter_amount();
	driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@text='SEND MONEY']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@text='RESEND CODE']")).click();
	Thread.sleep(4000);
	
}

public void verify_transaction_when_insufficient_fund () throws InterruptedException{
	try{
	Thread.sleep(3000);
	   touchAction.press(718,330).release().perform();
	   Thread.sleep(3000);

	  String balance= driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/cv_balance_container']//*[@resource-id='com.salarium.android.salpay:id/tv_amount']")).getText();
	  balance = balance.replaceAll("[\\D.]", "");
	  String balance1=balance.substring(0, balance.length() - 2);
	  //System.out.println(balance1);
	  
	  int Balance_int = Integer.parseInt(balance1);
	  System.out.println(Balance_int);
	  int number_to_add=5;
	  int add=Balance_int+number_to_add;
	  String add_string= String.valueOf(add);
	  System.out.println(add_string);
	    ((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
		Thread.sleep(4000);
		touchAction.press(93,1356).release().perform();
		Thread.sleep(4000);
		touchAction.press(300,726).release().perform();
		Thread.sleep(4000);
		SendMoney sendmoneyoption = new SendMoney();
		sendmoneyoption.enter_accountname();
		sendmoneyoption.enter_account_number();
		driver.findElement(By.xpath("//*[@text='Amount']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys(add_string);
		((AndroidDriver<?>) driver).hideKeyboard();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@text='SEND MONEY']")).click();
	Thread.sleep(4000);
	if(driver.findElement(By.xpath("//*[@text='SEND MONEY']")).isDisplayed()){
		System.out.println("Verification successful if entering more than available amount");
		Log.info("Verification successful if entering more than available amount");
	}
	else{
		System.out.println("Verification unsuccessful if entering more than available amount");
		Log.info("Verification unsuccessful if entering more than available amount");
	}
	}catch(Exception e){
		
	}
	
	((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
	
}

public void Enter_correct_OTP() throws InterruptedException{
	
	
	Thread.sleep(4000);
	touchAction.press(93,1356).release().perform();
	Thread.sleep(4000);
	touchAction.press(300,726).release().perform();
	Thread.sleep(4000);
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.enter_accountname();

	sendmoneyoption.enter_account_number();
	sendmoneyoption.enter_amount();
	driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@text='SEND MONEY']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@class='android.widget.EditText']")).click();
	Thread.sleep(4000);
	((AndroidDriver<?>) driver).getKeyboard().sendKeys("0000");
	Thread.sleep(4000);
	if(driver.findElement(By.xpath("//*[@class='android.widget.FrameLayout']")).isDisplayed())
	{
		System.out.println("Transaction message coming properly");
		Log.info("Transaction message coming properly");
	}
	else{
		System.out.println("Transaction message not coming properly");
		Log.info("Transaction message not coming properly");
        }
	driver.findElement(By.xpath("//*[@text='VIEW TRANSACTION']")).click();
	Thread.sleep(3000);
	if(driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/cv_balance_container']//*[@resource-id='com.salarium.android.salpay:id/tv_amount']")).isDisplayed())
	{
		System.out.println("view Transaction button working properly");
		Log.info("view Transaction button working properly");
	}
	else{
		System.out.println("view Transaction button not working properly");
		Log.info("view Transaction button not working properly");

	}
	((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);

}

public void Return_to_home_screen() throws InterruptedException{
	int Balance_int= 0;
	
	try{
	Thread.sleep(3000);
	   touchAction.press(718,330).release().perform();
	   Thread.sleep(3000);

	  String balance= driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/cv_balance_container']//*[@resource-id='com.salarium.android.salpay:id/tv_amount']")).getText();
	  balance = balance.replaceAll("[\\D.]", "");
	  String balance1=balance.substring(0, balance.length() - 2);
	  //System.out.println(balance1);
	  
	  Balance_int = Integer.parseInt(balance1);
	  System.out.println(Balance_int);
	  int amount= 1000;
	  String amount1= "1000";
	  String sentamount= String.valueOf(amount);
	  int final_amount=Balance_int-amount-45;
	  String finalamount= String.valueOf(final_amount);
		System.out.println(finalamount);
	    ((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
		Thread.sleep(4000);
		touchAction.press(93,1356).release().perform();
		Thread.sleep(4000);
		touchAction.press(300,726).release().perform();
		Thread.sleep(4000);
		SendMoney sendmoneyoption = new SendMoney();
		sendmoneyoption.enter_accountname();
		sendmoneyoption.enter_account_number();
		driver.findElement(By.xpath("//*[@text='Amount']//*[@resource-id='com.salarium.android.salpay:id/fieldEditText']")).sendKeys(amount1);
		((AndroidDriver<?>) driver).hideKeyboard();
		Thread.sleep(3000);

		driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@text='SEND MONEY']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@class='android.widget.EditText']")).click();
	Thread.sleep(4000);
	((AndroidDriver<?>) driver).getKeyboard().sendKeys("0000");
	Thread.sleep(2000);

	((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
	Thread.sleep(3000);

	if(driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/rl_buy_load_container']")).isDisplayed()){
		System.out.println("Salpay menu screen visible");
		Log.info("Salpay menu screen visible");
	}
	else{
		System.out.println("Salpay menu screen not visible");
		Log.info("Salpay menu screen not visible");
}

	   Thread.sleep(3000);

	touchAction.press(718,330).release().perform();
	   Thread.sleep(3000);

	  String restamount= driver.findElement(By.xpath("//*[@resource-id='com.salarium.android.salpay:id/cv_balance_container']//*[@resource-id='com.salarium.android.salpay:id/tv_amount']")).getText();
	  restamount = restamount.replaceAll("[\\D.]", "");
	  String restamount1=restamount.substring(0, balance.length() - 2);
	  //System.out.println(balance1);
	  System.out.println(restamount1);

	if(restamount1.equals(finalamount)){
		System.out.println("Balance updated properly");
		Log.info("Balance updated properly");
	}
	else{
		System.out.println("Balance not updated properly");
		Log.info("Balance not updated properly");
	}
}catch (Exception e){
	   e.printStackTrace();	
	
}
	((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
	
}

public void verify_transaction_history() throws InterruptedException{
	
	Thread.sleep(4000);
	touchAction.press(93,1356).release().perform();
	Thread.sleep(4000);
	touchAction.press(300,726).release().perform();
	Thread.sleep(4000);
	SendMoney sendmoneyoption = new SendMoney();
	sendmoneyoption.enter_accountname();

	sendmoneyoption.enter_account_number();
	sendmoneyoption.enter_amount();
	driver.findElement(By.xpath("//*[@text='CALCULATE FEES']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@text='SEND MONEY']")).click();
	Thread.sleep(4000);
	driver.findElement(By.xpath("//*[@class='android.widget.EditText']")).click();
	Thread.sleep(4000);
	((AndroidDriver<?>) driver).getKeyboard().sendKeys("0000");
	Thread.sleep(3000);
	((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
	Thread.sleep(3000);
	touchAction.press(718,330).release().perform();
	   Thread.sleep(3000);
	   
		MobileElement Bank_name= driver.findElement(By.xpath("//*[@class='android.widget.ListView']"));
		if(Bank_name.isDisplayed()){
			System.out.println("All the details coming properly in transaction history");
			Log.info("All the details coming properly in transaction history");
		}
		else{
			System.out.println("All the details not coming properly in transaction history");
			Log.info("All the details not coming properly in transaction history");
		}
	   
	   }

           public void backbutton() throws InterruptedException{
			Thread.sleep(4000);	
			((AndroidDriver<?>) driver).pressKeyCode(AndroidKeyCode.BACK);
			Thread.sleep(4000);

		}
	

}
