package com.sg.mobiletest.Mobiletesting;



import org.junit.runner.RunWith;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;

import cucumber.api.CucumberOptions;




@RunWith(ExtendedCucumber.class)
@CucumberOptions(
		features = {"src/test/java"},
		plugin = {"pretty","html:target/cucumber-html-report",
				"junit:target/cucumber-reports/Cucumber.xml",
		"junit:target/cucumber-junit-report/allcukes.xml",
		"json:target/cucumber-report.json",
		"json:target/cucumber.json",
		"usage:target/cucumber-usage.json"},
		//tags = {" @SaveContractMBCA "}
		
	monochrome= true
		)

public class Runner {
	
	
	/*@BeforeTest
	public static void meth2() throws Throwable {
		
		
		Open_portal.open_portal();
		
	}
	*/
	
	public static void main(String[] args)
	{
		System.out.println("***##!!!!!!!!!!!!####***");
	    try {
			cucumber.api.cli.Main.main(args);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	  
	}
}
	


