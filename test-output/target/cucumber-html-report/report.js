$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("mobiletest.feature");
formatter.feature({
  "line": 2,
  "name": "Mobile",
  "description": "Testing the Send Money functionality",
  "id": "mobile",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@tag"
    }
  ]
});
formatter.scenarioOutline({
  "line": 6,
  "name": "",
  "description": "",
  "id": "mobile;",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 5,
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "Open App",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "Login using \"\u003cUserName\u003e\" and \"\u003cPassword\u003e\" click ok",
  "keyword": "Given "
});
formatter.step({
  "comments": [
    {
      "line": 9,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_001\u003d\u003d\u003d\u003d\u003d#"
    }
  ],
  "line": 10,
  "name": "Verify Send Money button on quick options",
  "keyword": "Then "
});
formatter.examples({
  "comments": [
    {
      "line": 11,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_002\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 12,
      "value": "#Then Verify Send Money Options"
    },
    {
      "line": 13,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_003\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 14,
      "value": "#Then Verify Back icon on the send money options page"
    },
    {
      "line": 15,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_004\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 16,
      "value": "#Then Verify send money to bank form"
    },
    {
      "line": 17,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_005\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 18,
      "value": "#Then Verify Back icon on the send money form page"
    },
    {
      "line": 19,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_005\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 20,
      "value": "#Then Verify Cancel icon on the send money page(--no cancel icon exists)"
    },
    {
      "line": 21,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_006\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 22,
      "value": "#Then Verify List of Banks"
    },
    {
      "line": 23,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_007\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 24,
      "value": "#Then Verify Additional Information icon on Account Name"
    },
    {
      "line": 25,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_008\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 26,
      "value": "#Then Verify Additional Information icon on Account Number"
    },
    {
      "line": 27,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_009\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 28,
      "value": "#Then Verify Amount Number format"
    },
    {
      "line": 29,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_010\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 30,
      "value": "#Then Verify Clear icon when Amount text field is blank(--clear icon element not defined by the development team)"
    },
    {
      "line": 31,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_011\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 32,
      "value": "#Then Verify Clear icon when Amount is filled Account used:    (--clear icon element not defined by the development team)"
    },
    {
      "line": 33,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_012\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 34,
      "value": "#Then Tap CALCULATE FEES button when send money form is blank"
    },
    {
      "line": 35,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_013\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 36,
      "value": "#Then Tap CALCULATE FEES button when Bank Name is not selected(--By default\"Banco de oro\" bank is selected)"
    },
    {
      "line": 37,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_014\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 38,
      "value": "#Then Tap CALCULATE FEES button when Account Name is blank"
    },
    {
      "line": 39,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_015\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 40,
      "value": "#Then Tap Verify Allowed Cahracter input on Account Name field"
    },
    {
      "line": 41,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_016\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 42,
      "value": "#Then Tap CALCULATE FEES button when Account Number is blank"
    },
    {
      "line": 43,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_017\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 44,
      "value": "#Then Tap CALCULATE FEES button when Account Number is less than or greater than the required digits"
    },
    {
      "line": 45,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_018\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 46,
      "value": "#Then Tap CALCULATE FEES button when Amount is blank"
    },
    {
      "line": 47,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_019\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 48,
      "value": "#Then Tap CALCULATE FEES button when Amount Entered is less than or greater than the required value"
    },
    {
      "line": 49,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_020\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 50,
      "value": "#Then Tap Send Money when SALPAy balance is insufficient to send the Amount"
    },
    {
      "line": 51,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_021\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 52,
      "value": "#Then Change bank name when Send money Form is filled"
    },
    {
      "line": 53,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_022\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 54,
      "value": "#Then Verify processing details"
    },
    {
      "line": 55,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_023\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 56,
      "value": "#Then Update Account name when processing fee is already calculated"
    },
    {
      "line": 57,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_024\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 58,
      "value": "#Then Update Account Number when processing fee is already calculated"
    },
    {
      "line": 59,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_025\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 60,
      "value": "#Then Update Amount when processing fee is already calculated"
    },
    {
      "line": 61,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_026\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 62,
      "value": "#Then Send money verification page"
    },
    {
      "line": 63,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_027\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 64,
      "value": "#Then Verify Back icon on the send money verification page"
    },
    {
      "line": 65,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_028\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 66,
      "value": "#Then Resubmit the Send Money form"
    },
    {
      "line": 67,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_029\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 68,
      "value": "#Then Verify Cancel icon on the send money verification page #(--  No cancel button exists)"
    },
    {
      "line": 69,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_031\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 70,
      "value": "#Then Input incorrect verification code"
    },
    {
      "line": 71,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_032\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 72,
      "value": "#Then Verify Resend link text  #(--can\u0027t verify as the resend otp acknowledgement message is not locatable)"
    },
    {
      "line": 73,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_033\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 74,
      "value": "#Then Input the old verification code   #(--can\u0027t verify as we dont have old otp)"
    },
    {
      "line": 75,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_034\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 76,
      "value": "#Then Verify Transaction when account Balance is insufficient"
    },
    {
      "line": 77,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_035\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 78,
      "value": "#Then Input correct verification code"
    },
    {
      "line": 79,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_036\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 80,
      "value": "#Then Return to home screen"
    },
    {
      "line": 81,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_037\u003d\u003d\u003d\u003d\u003d#"
    },
    {
      "line": 82,
      "value": "#Then Verify Transaction History"
    },
    {
      "line": 84,
      "value": "# Then Click on Send Money"
    },
    {
      "line": 85,
      "value": "# Then calculate fees"
    }
  ],
  "line": 86,
  "name": "",
  "description": "",
  "id": "mobile;;",
  "rows": [
    {
      "cells": [
        "UserName",
        "Password"
      ],
      "line": 87,
      "id": "mobile;;;1"
    },
    {
      "cells": [
        "korina.sanga+ssp1@salarium.com",
        "1234"
      ],
      "line": 88,
      "id": "mobile;;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 88,
  "name": "",
  "description": "",
  "id": "mobile;;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@tag"
    },
    {
      "line": 5,
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "Open App",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "Login using \"korina.sanga+ssp1@salarium.com\" and \"1234\" click ok",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Given "
});
formatter.step({
  "comments": [
    {
      "line": 9,
      "value": "#\u003d\u003d\u003d\u003d\u003dSALPAY_SM_001\u003d\u003d\u003d\u003d\u003d#"
    }
  ],
  "line": 10,
  "name": "Verify Send Money button on quick options",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.open_App()"
});
formatter.result({
  "duration": 54689392702,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "korina.sanga+ssp1@salarium.com",
      "offset": 13
    },
    {
      "val": "1234",
      "offset": 50
    }
  ],
  "location": "Steps.login_using_and_click_ok(String,String)"
});
formatter.result({
  "duration": 53403663244,
  "status": "passed"
});
formatter.match({
  "location": "Steps.verify_Send_Money_button_on_quick_options()"
});
formatter.result({
  "duration": 10013328068,
  "status": "passed"
});
});