$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("mobiletest.feature");
formatter.feature({
  "line": 2,
  "name": "Mobile",
  "description": "Testing the mobile login",
  "id": "mobile",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@tag"
    }
  ]
});
formatter.scenarioOutline({
  "line": 6,
  "name": "",
  "description": "",
  "id": "mobile;",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 5,
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "Open App",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "Login using \"\u003cUserName\u003e\" and \"\u003cPassword\u003e\" click ok",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "Click on Send Money",
  "keyword": "Then "
});
formatter.examples({
  "line": 10,
  "name": "",
  "description": "",
  "id": "mobile;;",
  "rows": [
    {
      "cells": [
        "UserName",
        "Password"
      ],
      "line": 11,
      "id": "mobile;;;1"
    },
    {
      "cells": [
        "korina.sanga+ssp1@salarium.com",
        "1234"
      ],
      "line": 12,
      "id": "mobile;;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 12,
  "name": "",
  "description": "",
  "id": "mobile;;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@tag"
    },
    {
      "line": 5,
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "Open App",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "Login using \"korina.sanga+ssp1@salarium.com\" and \"1234\" click ok",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "Click on Send Money",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});